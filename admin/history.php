<?php include_once '../include/function.php'; 
logged_in();
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Withdraw History</title>
  <link rel="shortcut icon" href="../images/favicon.png">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../user/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../user/css/adminlte.min.css">
  <link rel="stylesheet" type="text/css" href="../user/css/style.css">
</head>

<body class="hold-transition layout-top-nav">
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
      <div class="container">
        <a href="./" class="navbar-brand">
          <img src="../user/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
          <span class="brand-text font-weight-light"><?php echo SITENAME ?></span>
        </a>

        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse"
          aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
          <!-- Left navbar links -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a href="users" class="nav-link"><i class="fa fa-users"></i> Users</a>
            </li>
            <li class="nav-item">
              <a href="wallet" class="nav-link"><i class="fa fa-btc"></i> Wallet</a>
            </li>
          </ul>
        </div>

        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
          <li class="nav-item">
            <a class="nav-link text-danger" href="logout">
              <i class="fa fa-sign-out"></i> Logout
            </a>
          </li>
        </ul>
      </div>
    </nav>
    <!-- /.navbar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark"><?php Greeting() ?> <i class="fa fa-moon-o"></i>
                <span><?php echo $_SESSION['fname'] ?></span></h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container">
          <!-- Small boxes (Stat box) -->
          <?php echo Error_Message(); echo Success_Message(); ?>
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Withdraw History</h3>

              <div class="card-tools">
                <div>
                  <button class="btn btn-success" type="button" data-toggle="modal" data-target="#add"><i
                      class="fa fa-pencil"></i> Add</button>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
              <table class="table table-hover text-nowrap">
                <thead>
                  <tr>
                    <th>Amount</th>
                    <th>Withdrawn Date</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 

                    $id = $_GET['his'];

                    $withdraw = "SELECT * FROM withdrawn WHERE user_id = '$id' ";
                    $withdrawdb = query($withdraw);

                    while ($row = fetch_all($withdrawdb)) {
                      $amount = $row['amount'];
                      $date = $row['date'];
                      $active = $row['active'];

                      echo "<tr>";
                      echo "<td>$$amount</td>";
                      echo "<td>$date</td>";
                      if ($active == 0) {
                        echo "<td><span class=\"text-danger\">Pending</span></td>";
                      } else {
                        echo "<td><span class=\"text-success\">Approved</span></td>";
                      }
                      echo "</tr>";

                    }

                    $pending = "SELECT * FROM pending WHERE user_id = '$id' ";
                    $pendingdb = query($pending);

                    while ($row = fetch_all($pendingdb)) {
                      $amount = $row['amount'];
                      $date = $row['date'];
                      $active = $row['active'];

                      echo "<tr>";
                      echo "<td>$$amount</td>";
                      echo "<td>$date</td>";
                      if ($active == 0) {
                        echo "<td><span class=\"text-danger\">Pending</span></td>";
                      } else {
                        echo "<td><span class=\"text-success\">Approved</span></td>";
                      }
                      echo "</tr>";

                    }

                    if (isset($_POST['withadd'])) {
                      $amount = $_POST['amount'];
                      $date = $_POST['date'];

                      if (empty($amount && $date)) {
                        $_SESSION['ErrorMessage'] = "Amount or Date can't be empty";
                        Redirect('history?his='.$id);
                      } else {
                        $withdrawn = "INSERT INTO withdrawn(user_id, amount, date, active) VALUES('$id', '$amount', '$date', 1)";
                        query($withdrawn);

                        $_SESSION['SuccessMessage'] = "Successfully Added";
                        Redirect('history?his='.$id);
                      }
                    }
                     ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="add">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Withdrawn</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="modal-body">
              <form action="" method="post">
                <div class="form-group">
                  <label>Amount</label>
                  <input type="number" name="amount" placeholder="1,000" class="form-control">
                </div>
                <div class="form-group">
                  <label>Date</label>
                  <input type="date" name="date" class="form-control">
                </div>
                <div class="text-center">
                  <button type="submit" name="withadd" class="btn btn-success btn-lg btn-block">Withdrawn</button>
                </div>
              </form>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
      <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
      </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="float-right d-none d-sm-inline">
        Crypto World
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; 2020 All rights reserved.
    </footer>
  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->

  <!-- jQuery -->
  <script src="../user/js/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="../user/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../user/js/adminlte.min.js"></script>
</body>

</html>
