<?php include_once '../include/function.php'; 
logged_in(); ?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Wallet</title>
  <link rel="shortcut icon" href="../images/favicon.png">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../user/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../user/css/adminlte.min.css">
  <link rel="stylesheet" type="text/css" href="../user/css/style.css">
  <link rel="stylesheet" type="text/css" href="../user/css/toastr.min.css">
</head>

<body class="hold-transition layout-top-nav">
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
      <div class="container">
        <a href="./" class="navbar-brand">
          <img src="../user/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
          <span class="brand-text font-weight-light"><?php echo SITENAME ?></span>
        </a>

        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse"
          aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
          <!-- Left navbar links -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a href="users" class="nav-link"><i class="fa fa-users"></i> Users</a>
            </li>
            <li class="nav-item">
              <a href="wallet" class="nav-link"><i class="fa fa-btc"></i> Wallet</a>
            </li>
          </ul>
        </div>

        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
          <li class="nav-item">
            <a class="nav-link text-danger" href="logout">
              <i class="fa fa-sign-out"></i> Logout
            </a>
          </li>
        </ul>
      </div>
    </nav>
    <!-- /.navbar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark"><?php Greeting() ?> <i class="fa fa-moon-o"></i>
                <span><?php echo $_SESSION['fname'] ?></span></h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-4">
              <?php echo Error_Message(); echo Success_Message(); 

              $wallet = "SELECT btc FROM wallet";
              $walletsel = query($wallet);

              while ($row = fetch_all($walletsel)) {
                $btc = $row['btc'];
              }
              ?>
              <div class="card mb-4 shadow-sm">
                <div class="card-header">
                  <div class="row">
                    <div class="col-md-6 col-6">
                      <h4 class="my-0 font-weight-normal">Wallet</h4>
                    </div>
                    <div class="col-md-6 col-6 text-right">
                      <button class="btn btn-success" type="button" data-toggle="modal" data-target="#bitcoin">Add /
                        Edit</button>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div style="border: 2px lightgray solid; width: 210px; height: 210px; margin: 0 auto">
                    <?php if (empty($btc)) {
                      echo "No Bitcoin Address";
                    } else {
                      echo '<img src="temp/'.$btc.'.png" style="width:200px; height:200px;">';
                    } ?>
                  </div>
                </div>
                <h6 class="text-center">BitCoin Wallet Address</h6>
                <h6 class="text-center font-weight-bold copyMe"><?php if (empty($btc)) {
                  echo "No Bitcoin Address";
                } else {
                  echo $btc;
                } ?></h6>
                <div class="text-center mb-4">
                  <button class="btn btn-default copybtn">Copy Address</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
      <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
      </div>
    </aside>
    <!-- /.control-sidebar -->

    <div class="modal fade" id="bitcoin">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <?php bitCoin() ?>
          <div class="modal-body">
            <h6 class="text-center">BitCoin Wallet Address</h6>
            <form action="" method="post">
              <div class="form-group">
                <input class="form-control" type="text" name="btc" placeholder="Bitconin Address">
              </div>
              <div class="text-center">
                <button type="submit" class="btn btn-success btn-lg btn-block my-3" name="wallet">Wallet Add</button>
              </div>
            </form>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="float-right d-none d-sm-inline">
        Crypto World
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; 2020 All rights reserved.
    </footer>
  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->

  <!-- jQuery -->
  <script src="../user/js/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="../user/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../user/js/adminlte.min.js"></script>
  <script type="text/javascript" src="../user/js/toastr.min.js"></script>

  <script>
    var copyEmailBtn = document.querySelector('.copybtn');
    copyEmailBtn.addEventListener('click', function (event) {
      // Select the email link anchor text  
      var emailLink = document.querySelector('.copyMe');
      var range = document.createRange();
      range.selectNode(emailLink);
      window.getSelection().addRange(range);

      try {
        // Now that we've selected the anchor text, execute the copy command  
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        toastr.success('BTC Address Copy was ' + msg);
      } catch (err) {
        toastr.success('Oops, unable to copy');
      }

      // Remove the selections - NOTE: Should use
      // removeRange(range) when it is supported  
      window.getSelection().removeAllRanges();
    });
  </script>
</body>

</html>
