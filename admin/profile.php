<?php include_once '../include/function.php'; 
logged_in();
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Profile</title>
  <link rel="shortcut icon" href="../images/favicon.png">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../user/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../user/css/adminlte.min.css">
  <link rel="stylesheet" type="text/css" href="../user/css/style.css">
  <link rel="stylesheet" type="text/css" href="../user/css/toastr.min.css">
</head>

<body class="hold-transition layout-top-nav">
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
      <div class="container">
        <a href="./" class="navbar-brand">
          <img src="../user/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
          <span class="brand-text font-weight-light"><?php echo SITENAME ?></span>
        </a>

        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse"
          aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
          <!-- Left navbar links -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a href="users" class="nav-link"><i class="fa fa-users"></i> Users</a>
            </li>
            <li class="nav-item">
              <a href="wallet" class="nav-link"><i class="fa fa-btc"></i> Wallet</a>
            </li>
          </ul>
        </div>

        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
          <li class="nav-item">
            <a class="nav-link text-danger" href="logout">
              <i class="fa fa-sign-out"></i> Logout
            </a>
          </li>
        </ul>
      </div>
    </nav>
    <!-- /.navbar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark"><?php Greeting() ?> <i class="fa fa-moon-o"></i>
                <span><?php echo $_SESSION['fname'] ?></span></h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
      <!-- Main content -->
      <section class="content">
        <div class="container">
          <?php 

        echo Error_Message(); echo Success_Message();

        global $capamount, $earamount, $refamount, $withamount;
        $profileid = escape(trim($_GET['view']));
        $date = date("d-M-Y");

        // Capital Row
        $capsel = "SELECT * FROM capital WHERE user_id = '$profileid' ";
        $capquery = query($capsel);
        while ($row = fetch_all($capquery)) {
          $capamount = $row['amount'];
        }

        // Earning Row
        $earsel = "SELECT * FROM earning WHERE user_id = '$profileid' ";
        $earquery = query($earsel);
        while ($row = fetch_all($earquery)) {
          $earamount = $row['amount'];
        }

        // Referral Row
        $refsel = "SELECT * FROM referral WHERE user_id = '$profileid' ";
        $refquery = query($refsel);
        while ($row = fetch_all($refquery)) {
          $refamount = $row['amount'];
        }

        // Withdrawn Row
        $withsel = "SELECT SUM(amount) AS sum FROM withdrawn WHERE user_id = '$profileid' ";
        $withquery = query($withsel);
        while ($row = fetch_all($withquery)) {
          $withamount = $row['sum'];
        }




        // Capital Section
        if (isset($_POST['capfadd'])) {
          $userid = $_GET['view'];
          $amount = $_POST['captital'];
          if (empty($amount)) {
            $_SESSION['ErrorMessage'] = "Captital Amount can't be empty";
            Redirect('profile?view='.$profileid);
          } else {
            
            $cap = "INSERT INTO capital(user_id, amount, date) VALUES('$profileid', '$amount', '$date') ON DUPLICATE KEY UPDATE amount = amount + '$amount' ";
            $capital = query($cap);

            $_SESSION['SuccessMessage'] = "Captital Added Successfully";
            Redirect('profile?view='.$profileid);
          }
        }

        // Earnings Section
        if (isset($_POST['earadd'])) {
          $userid = $_GET['view'];
          $amount = $_POST['earning'];
          if (empty($amount)) {
            $_SESSION['ErrorMessage'] = "Earning Amount can't be empty";
            Redirect('profile?view='.$profileid);
          } else {

            $ear = "INSERT INTO earning(user_id, amount, date) VALUES('$profileid', '$amount', '$date') ON DUPLICATE KEY UPDATE amount = amount + '$amount', date = '$date' ";
            $capital = query($ear);

            $_SESSION['SuccessMessage'] = "Earning Added Successfully";
            Redirect('profile?view='.$profileid);
          }
        }

        //Referral Section
        if (isset($_POST['refadd'])) {
          $userid = $_GET['view'];
          $amount = $_POST['referral'];
          if (empty($amount)) {
            $_SESSION['ErrorMessage'] = "Referral Amount can't be empty";
            Redirect('profile?view='.$profileid);
          } else {

            $ref = "INSERT INTO referral(user_id, amount, date) VALUES('$profileid', '$amount', '$date') ON DUPLICATE KEY UPDATE amount = amount + '$amount'";
            $capital = query($ref);

            $_SESSION['SuccessMessage'] = "Referral Added Successfully";
            Redirect('profile?view='.$profileid);
          }
        }

        if (isset($_POST['reset'])) {
          $reset1 = "DELETE FROM capital WHERE user_id = '$profileid' ";
          query($reset1);

          $reset2 = "DELETE FROM earning WHERE user_id = '$profileid' ";
          query($reset2);

          $reset3 = "DELETE FROM pending WHERE user_id = '$profileid' ";
          query($reset3);

          $reset4 = "DELETE FROM referral WHERE user_id = '$profileid' ";
          query($reset4);

          $reset5 = "DELETE FROM withdrawn WHERE user_id = '$profileid' ";
          query($reset5);

          $_SESSION['SuccessMessage'] = "Account Reseted Successfully";
          Redirect('profile?view='.$profileid);
        }


        if (isset($_POST['delete'])) {
          $delete = "DELETE FROM user WHERE id = '$profileid' ";
          query($delete);

          $_SESSION['SuccessMessage'] = "Account Deleted Successfully";
          Redirect('index');
        }


         ?>
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-6 my-3">
              <div class="row">
                <div class="col-md-6 col-6">
                  <div class="small-box bg-info">
                    <div class="inner">
                      <h3>$<?php if (empty($capamount)) {
                  echo "0";
                } else {
                  echo $capamount;
                } ?></h3>
                      <p>Current Capitals</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-6">
                  <div class="small-box bg-success">
                    <div class="inner">
                      <h3>$<?php if (empty($earamount)) {
                  echo "0";
                } else {
                  echo $earamount;
                } ?></h3>
                      <p>Current Earnings</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-6">
                  <div class="small-box bg-warning">
                    <div class="inner">
                      <h3>$<?php if (empty($refamount)) {
                  echo "0";
                } else {
                  echo $refamount;
                } ?></h3>
                      <p>Referral Bonus</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-6">
                  <div class="small-box bg-danger">
                    <div class="inner">
                      <h3>$<?php if (empty($withamount)) {
                  echo "0";
                } else {
                  echo $withamount;
                } ?></h3>
                      <p>Withdrawn</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-6">
                  <form action="" method="post">
                  <button class="btn btn-primary btn-lg btn-block" type="submit" name="reset" onClick="javascript: return confirm('Are you sure you want to Reset Account')"><i class="fa fa-refresh"></i> Reset Account</button>
                  </form>
                </div>
                <div class="col-md-6 col-6">
                  <form action="" method="post">
                  <button class="btn btn-danger btn-lg btn-block" type="submit" name="delete" onClick="javascript: return confirm('Are you sure you want to Delete Account')"><i class="fa fa-close"></i> Delete
                    Account</button>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Top Up</h3>

                      <div class="card-tools">
                      </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                      <table class="table table-hover text-nowrap">
                        <thead>
                          <tr>
                            <th>Capitals</th>
                            <th>Earnings</th>
                            <th>Referral </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><button class="btn btn-info" type="button" data-toggle="modal"
                                data-target="#Capitals">Top Up</button></td>
                            <td><button class="btn btn-success" type="button" data-toggle="modal"
                                data-target="#Earnings">Top Up</button></td>
                            <td><button class="btn btn-warning" type="button" data-toggle="modal"
                                data-target="#Referral">Top Up</button></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Withdrawer Request</h3>

                      <div class="card-tools">
                        <div class="">
                          <a href="<?php echo "history?his=$profileid" ?>"><button class="btn btn-info"><i
                                class="fa fa-pencil"></i> Withdraw History</button></a>
                        </div>
                      </div>

                      <div class="card-tools">
                      </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                      <table class="table table-hover text-nowrap">
                        <thead>
                          <tr>
                            <th>Amount</th>
                            <th>Withdrawn Date</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 

                    $withdraw = "SELECT * FROM pending WHERE user_id = '$profileid' AND active = 0 ";
                    $withdrawnsel = query($withdraw);

                    while ($row = fetch_all($withdrawnsel)) {
                      $amount = $row['amount'];
                      $date = $row['date'];

                      echo "<tr>";
                      echo "<td>$$amount</td>";
                      echo "<td>$date</td>";
                      echo "<td><button class=\"btn btn-success\" type=\"button\" data-toggle=\"modal\" data-target=\"#approve\">Approve</button></td>";
                      echo "<form method=\"post\">";
                      echo "<td><button class=\"btn btn-danger\" type='submit' name='decline'>Decline</button></td>";
                      echo "</form>";
                      echo "</tr>";
                    }

                     ?>
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                </div>
              </div>
            </div>
            <!-- ./col -->
          </div>
          <!-- Next in line -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
      <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
      </div>
    </aside>
    <!-- /.control-sidebar -->
    <div class="modal fade" id="approve">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <?php
            $date = date("Y-m-d");
            $callout = "SELECT * FROM pending WHERE user_id = '$profileid' ";
            $callsel = query($callout);
            while ($row = fetch_all($callsel)) {
              $bankname = $row['bankname'];
              $accname = $row['accname'];
              $accnumber = $row['accnumber'];
              $add = $row['btc'];
              $amount = $row['amount'];
             } 

            if (isset($_POST['approve'])) {

              $approve = "INSERT INTO withdrawn(user_id, amount, date, active) VALUES('$profileid', '$amount', '$date', 1)";
              query($approve);

              $del = "DELETE FROM pending WHERE user_id = '$profileid' ";
              $deldb = query($del);


              $_SESSION['SuccessMessage'] = "Successfully Approve";
              Redirect("profile?view=".$profileid);
            }

            if (isset($_POST['decline'])) {
              $decline = "INSERT INTO earning(user_id, amount, date) VALUES('$profileid', '$amount', '$date')";
              $declinesend = query($decline);

              $del = "DELETE FROM pending WHERE user_id = '$profileid' ";
              $deldb = query($del);


              $_SESSION['ErrorMessage'] = "Withdrawn Decline";
              Redirect("profile?view=".$profileid);
            }
             ?>
          <div class="modal-body">
            <!-- <img src="img/btc.jpeg" width="200" class="img-responsive center-block d-block mx-auto"> -->
            <?php if (empty($add)) {
                echo "<h6 class=\"text-center\">Bank Details</h6>";
                echo "<h5 class=\"text-center font-weight-bold\">$bankname</h5>";
                echo "<h5 class=\"text-center font-weight-bold\">$accname</h5>";
                echo "<h5 class=\"text-center font-weight-bold\">$accnumber</h5>";

              } else {
                echo "<h6 class=\"text-center\">BitCoin Wallet Address</h6>";
                echo "<h5 class=\"text-center font-weight-bold copyMe\">$add</h5>";
                echo "<div class=\"text-center\">";
                echo "<button class=\"btn btn-default copybtn\">Copy Address</button>";
                echo "</div>";
              } ?>
            <form method="post" class="form-group my-1">
              <div class="row text-center">
                <div class="col-md-6 col-6">
                  <button class="btn btn-success form-control" type="submit" name="approve"><i class="fa fa-send"></i>
                    Approve</button>
                </div>
                <div class="col-md-6 col-6">
                  <button class="btn btn-danger form-control" type="submit" name="decline"><i class="fa fa-close"></i>
                    Approve</button>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="Capitals">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Capital Top Up</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="post">
              <div class="form-group">
                <input type="number" name="captital" placeholder="3000" class="form-control">
              </div>
              <div class="text-center">
                <button type="submit" name="capfadd" class="btn btn-info btn-lg btn-block">Top Up</button>
              </div>
            </form>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="Earnings">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Earnings Top Up</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="modal-body">
              <form action="" method="post">
                <div class="form-group">
                  <input type="number" name="earning" placeholder="1000" class="form-control">
                </div>
                <div class="text-center">
                  <button type="submit" name="earadd" class="btn btn-success btn-lg btn-block">Top Up</button>
                </div>
              </form>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="Referral">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Referral Top Up</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="post">
              <div class="form-group">
                <input type="number" name="referral" placeholder="600" class="form-control">
              </div>
              <div class="text-center">
                <button type="submit" name="refadd" class="btn btn-warning btn-lg btn-block">Top Up</button>
              </div>
            </form>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="float-right d-none d-sm-inline">
        Crypto World
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; 2020 All rights reserved.
    </footer>
  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->

  <!-- jQuery -->
  <script src="../user/js/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="../user/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../user/js/adminlte.min.js"></script>
  <script type="text/javascript" src="../user/js/toastr.min.js"></script>
</body>

</html>
