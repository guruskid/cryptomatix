<?php include_once 'include/function.php'; ?>
<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8" />
    <title><?php echo SITENAME ?> : Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.png">

    <!-- Template CSS Files -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/select2.min.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- Template JS Files -->
    <script src="js/modernizr.js"></script>

</head>

<body class="auth-page light">
    <!-- Wrapper Starts -->
    <div class="wrapper">
        <div class="container-fluid user-auth">
			<div class="hidden-xs col-sm-4 col-md-4 col-lg-4">
				<!-- Logo Starts -->
				<a class="logo" href="./">
					<img id="logo-user" class="img-responsive" src="images/logo-dark.png" alt="logo">
				</a>
				<!-- Logo Ends -->
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Logo Starts -->
				<a class="visible-xs" href="./">
					<img id="logo" class="img-responsive mobile-logo" width="200" src="images/logo-dark.png" alt="logo">
				</a>
				<!-- Logo Ends -->
				<div class="form-container">
					<div>
						<!-- Section Title Starts -->
						<div class="row text-center">
							<h2 class="title-head hidden-xs">get <span>started</span></h2>
							 <p class="info-form">Open account for free and start trading Bitcoins now!</p>
						</div>
						<form method="post" action="">
							<?php Register(); echo Error_Message(); echo Success_Message(); ?>
							<div class="row">
								<div class="form-group col-md-6">
									<input class="form-control" name="fname" id="fname" placeholder="First Name" type="text" value="<?php echo $fname ?>">
									<span class="invalid-feedback text-danger"><?php echo $fname_err ?></span>
								</div>
								<div class="form-group col-md-6">
									<input class="form-control" name="lname" id="lname" placeholder="Last Name" type="text" value="<?php echo $lname ?>">
									<span class="invalid-feedback text-danger"><?php echo $lname_err ?></span>
								</div>
								<div class="col-md-12 form-group">
									<input class="form-control" name="email" id="email" placeholder="Email Address" type="email" value="<?php echo $email ?>">
									<span class="invalid-feedback text-danger"><?php echo $email_err ?></span>
								</div>
								<div class="col-md-6 form-group">
									<input class="form-control" name="password" id="password" placeholder="Password" type="password">
									<span class="invalid-feedback text-danger"><?php echo $password_err ?></span>
								</div>
								<div class="col-md-6 form-group">
									<input class="form-control" name="cpassword" id="cpassword" placeholder="Confirm Password" type="password">
									<span class="invalid-feedback text-danger"><?php echo $cpassword_err ?></span>
								</div>

								<div class="col-md-12 form-group">
									<button class="btn btn-primary" type="submit">create account</button>
									<p class="text-center">already have an account ? <a href="login">Login</a>
								</div>
							</div>
						</form>
						<!-- Form Ends -->
					</div>
				</div>
			</div>
		</div>
        <!-- Template JS Files -->
        <script src="js/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/select2.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/custom.js"></script>
		
		<!-- Live Style Switcher JS File - only demo -->
		<script src="js/styleswitcher.js"></script>

    </div>
    <!-- Wrapper Ends -->
</body>


</html>