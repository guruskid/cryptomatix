<?php

// Query Function
function Query($query) {
    global $conn;
    return mysqli_query($conn, $query);
    confirm($query);
}

function fetch_all($result) {
    global $conn;
    return mysqli_fetch_array($result);
}

function escape($string) {
    global $conn;
    return mysqli_real_escape_string($conn, $string);
} 

function row_count($result) {
    global $conn;
    return mysqli_num_rows($result);
}
// Redirect Location
function Redirect($New_Location) {
    header("Location:" . $New_Location);
    exit;
}

function clean($string) {
    global $conn;
    return htmlentities($string);
}

function confirm($result) {
    global $conn;
    if (!$result) {
        die('Query Failed' .mysqli_error($conn));
    }
}

/// Error Message Echo
function Error_Message() {
    if (isset($_SESSION['ErrorMessage'])) {
        $Output = "<div class='alert alert-danger'>";
        $Output .= htmlentities($_SESSION['ErrorMessage']);
        $Output .= "</div>";
        $_SESSION['ErrorMessage'] = null;
        return $Output;
    }
}

/// Success Message Echo
function Success_Message() {
    if (isset($_SESSION['SuccessMessage'])) {
        $Output = "<div class='alert alert-success'>";
        $Output .= htmlentities($_SESSION['SuccessMessage']);
        $Output .= "</div>";
        $_SESSION['SuccessMessage'] = null;
        return $Output;
    }
}

// token generator
function token_gen() {
    $token = $_SESSION['token'] = md5(uniqid(mt_rand(), true));
    return $token;
}

// send email
function sendMail($email, $subject, $message) {

    $mail = new PHPMailer;

    $mail->isSMTP();
    $mail->Host = 'cryptomatrix.co';
    $mail->SMTPAuth = true; 
    $mail->Username = 'support@cryptomatrix.co';
    $mail->Password = 'Password@2020';
    $mail->SMTPSecure = 'ssl';
    $mail->Mailer = "smtp";
    $mail->SMTPKeepAlive = true;
    $mail->Port = 465;

    $mail->setFrom('support@cryptomatrix.co', 'CRYPTO MATRIX');
    $mail->addAddress($email);
    $mail->isHTML(true);


    $mail->Subject = $subject;
    $mail->Body    = $message;

    if(!$mail->send()) {
        return false;
    } else {
        return true;
    }   
    // return mail($email, $subject, $message);
}

function validate_ip($ip){
    if (strtolower($ip) === 'unknown')
        return false;

    $ip = ip2long($ip);

    // if the ip is set and not equivalent to 255.255.255.255
    if ($ip !== false && $ip !== -1) {
        /** make sure to get unsigned long representation of ip
        * due to discrepancies between 32 and 64 bit OSes and
        * signed numbers (ints default to signed in PHP) 
        */
        $ip = sprintf('%u', $ip);
        // do private network range checking
        if ($ip >= 0 && $ip <= 50331647) return false;
        if ($ip >= 167772160 && $ip <= 184549375) return false;
        if ($ip >= 2130706432 && $ip <= 2147483647) return false;
        if ($ip >= 2851995648 && $ip <= 2852061183) return false;
        if ($ip >= 2886729728 && $ip <= 2887778303) return false;
        if ($ip >= 3221225984 && $ip <= 3221226239) return false;
        if ($ip >= 3232235520 && $ip <= 3232301055) return false;
        if ($ip >= 4294967040) return false;
    }
    return true;
}

function getIP() {
    global $_SERVER;
    // check for shared internet/ISP IP
    if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }
 
    // check for IPs passing through proxies
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        // check if multiple ips exist in var
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip) {
                if (validate_ip($ip))
                    return $ip;
            }
        } else {
            if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR']))
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED']))
        return $_SERVER['HTTP_X_FORWARDED'];
    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
        return $_SERVER['HTTP_FORWARDED_FOR'];
    if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED']))
        return $_SERVER['HTTP_FORWARDED'];
    $ip=$_SERVER['REMOTE_ADDR'];
    $ip="::1"?"127.0.0.1":$ip;
    return $ip;
}


function getOS() { 

    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $os_platform    =   "Unknown OS Platform";

    $os_array       =   array(
        '/windows nt 10.0/i'     =>  'Windows 10',
        '/windows nt 6.3/i'     =>  'Windows 8.1',
        '/windows nt 6.2/i'     =>  'Windows 8',
        '/windows nt 6.1/i'     =>  'Windows 7',
        '/windows nt 6.0/i'     =>  'Windows Vista',
        '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
        '/windows nt 5.1/i'     =>  'Windows XP',
        '/windows xp/i'         =>  'Windows XP',
        '/windows nt 5.0/i'     =>  'Windows 2000',
        '/windows me/i'         =>  'Windows ME',
        '/win98/i'              =>  'Windows 98',
        '/win95/i'              =>  'Windows 95',
        '/win16/i'              =>  'Windows 3.11',
        '/macintosh|mac os x/i' =>  'Mac OS X',
        '/mac_powerpc/i'        =>  'Mac OS 9',
        '/linux/i'              =>  'Linux',
        '/ubuntu/i'             =>  'Ubuntu',
        '/iphone/i'             =>  'iPhone',
        '/ipod/i'               =>  'iPod',
        '/ipad/i'               =>  'iPad',
        '/android/i'            =>  'Android',
        '/blackberry/i'         =>  'BlackBerry',
        '/webos/i'              =>  'Mobile'
    );

    foreach ($os_array as $regex => $value) { 

        if (preg_match($regex, $user_agent)) {
            $os_platform    =   $value;
        }

    }   

    return $os_platform;

}

function getBrowser() {

    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $browser        =   "Unknown Browser";

    $browser_array  =   array(
        '/msie/i'       =>  'Internet Explorer',
        '/firefox/i'    =>  'Firefox',
        '/safari/i'     =>  'Safari',
        '/chrome/i'     =>  'Chrome',
        '/opera/i'      =>  'Opera',
        '/netscape/i'   =>  'Netscape',
        '/maxthon/i'    =>  'Maxthon',
        '/konqueror/i'  =>  'Konqueror',
        '/mobile/i'     =>  'Handheld Browser'
    );

    foreach ($browser_array as $regex => $value) { 

        if (preg_match($regex, $user_agent)) {
            $browser    =   $value;
        }

    }

    return $browser;

}


?>
