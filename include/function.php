<?php

///
ob_start();
session_start();

// Database
include_once 'database.php';
include_once 'core.php';
include_once 'PHPMailerAutoload.php';
include_once 'phpqrcode/qrlib.php';

// Registration
function Register() {
    global $fname_err, $lname_err, $email_err, $password_err, $cpassword_err;
    global $fname, $lname, $email, $password, $cpassword;

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $fname = escape(trim(ucfirst($_POST['fname'])));
        $lname = escape(trim(ucfirst($_POST['lname'])));
        $email = escape(trim($_POST['email']));
        $password = escape(trim($_POST['password']));
        $cpassword = escape(trim($_POST['cpassword']));
        date_default_timezone_set("Africa/Lagos");
        $date = date("h:i:s a m/d/Y");
        $role = "User";

        if (empty($fname)) {
            $fname_err = 'First Name is Required';
        } elseif (!preg_match("/^[a-zA-Z ]*$/",$fname)) {
            $fname_err = 'Only letters are allowed';
        }

        if (empty($lname)) {
            $lname_err = 'Last Name is Required';
        } elseif (!preg_match("/^[a-zA-Z ]*$/",$lname)) {
            $lname_err = 'Only letters are allowed';
        }

        if (empty($email)) {
            $email_err = 'Email Address is Required';
        } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $email_err = 'Invalid email format';
        } elseif (check_email($email)) {
            $email_err = 'Email Address Already Taken';
        }

        if (empty($password)) {
            $password_err = 'Password is Required';
        } elseif ($password != $cpassword) {
            $password_err = "Password Doesn't Match";
        }

        if (empty($cpassword)) {
            $cpassword_err = 'Confirm Password is Required';
        } elseif ($cpassword != $password) {
            $cpassword_err = "Confirm Password Doesn't Match";
        }

        if (empty($fname_err) && empty($lname_err) && empty($email_err) && empty($password_err) && empty($cpassword_err)) {

            $validate = md5($email . microtime());
            $password = password_hash($password, PASSWORD_DEFAULT);
            $data = "INSERT INTO user(fname, lname, email, password, date, confirm_code, role)
            VALUES('$fname', '$lname', '$email', '$password', '$date', '$validate', '$role')";

            query($data);
            $link = SITELINK;

            $subject = 'Registration Successfully';
            $message = "<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"UTF-8\">
</head>

<body>

    <head>
        <style>
            li {
                text-align: -webkit-match-parent;
                display: list-item;
            }
        </style>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8/\" />
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <style type=\"text/css\">
            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }
        </style>
        <style type=\"text/css\">
            @font-face {
                font-family: 'Google Sans';
                font-style: normal;
                font-weight: 400;
                mso-font-alt: Arial;
                src: local('Google Sans Regular'), local('GoogleSans-Regular'), url('https://fonts.gstatic.com/s/googlesans/v6/4UaGrENHsxJlGDuGo1OIlL3Owps.ttf') format('truetype');
            }
            
            @font-face {
                font-family: 'Google Sans';
                font-style: normal;
                font-weight: 500;
                mso-font-alt: Arial;
                src: local('Google Sans Medium'), local('GoogleSans-Medium'), url('https://fonts.gstatic.com/s/googlesans/v6/4UabrENHsxJlGDuGo1OIlLU94YtzCwM.ttf') format('truetype');
            }
            
            @font-face {
                font-family: 'Google Sans';
                font-style: normal;
                font-weight: 700;
                mso-font-alt: Arial;
                src: local('Google Sans Bold'), local('GoogleSans-Bold'), url('https://fonts.gstatic.com/s/googlesans/v6/4UabrENHsxJlGDuGo1OIlLV154tzCwM.ttf') format('truetype');
            }
            
            @font-face {
                font-family: 'Roboto';
                font-style: normal;
                font-weight: 400;
                mso-font-alt: Arial;
                src: local('Roboto'), local('Roboto-Regular'), url('https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxP.ttf') format('truetype');
            }
            
            @font-face {
                font-family: 'Roboto';
                font-style: normal;
                font-weight: 500;
                mso-font-alt: Arial;
                src: local('Roboto Medium'), local('Roboto-Medium'), url('https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fBBc9.ttf') format('truetype');
            }
            
            body {
                margin: 0 !important;
                padding: 0 !important;
                -webkit-text-size-adjust: 100% !important;
                -ms-text-size-adjust: 100% !important;
                -webkit-font-smoothing: antialiased !important;
            }
            
            img {
                border: 0 !important;
                outline: none !important;
            }
            
            p {
                Margin: 0px !important;
                Padding: 0px !important;
            }
            
            table {
                border-collapse: collapse;
                mso-table-lspace: 0px;
                mso-table-rspace: 0px;
            }
            
            td,
            a,
            span {
                border-collapse: collapse;
                mso-line-height-rule: exactly;
            }
            
            .ExternalClass * {
                line-height: 100%;
            }
            
            .em_defaultlink a {
                color: inherit;
                text-decoration: none;
            }
            
            #footerUnsubText a {
                color: #9aa0a6!important;
            }
            
            span.MsoHyperlink {
                mso-style-priority: 99;
                color: inherit;
            }
            
            span.MsoHyperlinkFollowed {
                mso-style-priority: 99;
                color: inherit;
            }
            
            .em_white a {
                color: #ffffff;
                text-decoration: none;
            }
            
            @media only screen and (max-width: 599px) {
                .premier-logo {
                    display: inline-block !important;
                    width: 25%;
                    padding: 4%
                }
                .premier-logo img {
                    width: 100% !important;
                    height: auto !important;
                }
                .hom {
                    display: none !important;
                }
                .mw_full_img {
                    max-width: 100% !important;
                    height: auto !important;
                }
                .em_wrapper_50_1 {
                    width: 46% !important;
                    margin: 0px 5px !important;
                }
                .em_main_table {
                    width: 480px !important;
                }
                .em_wrapper {
                    width: 100% !important;
                }
                .em_hide {
                    display: none!important;
                }
                .em_full_img {
                    width: 100% !important;
                    height: auto !important;
                    max-width: none !important;
                }
                .em_half_img {
                    width: 199px !important;
                    height: auto !important;
                    max-width: none !important;
                    display: inline-block!important;
                }
                .em_height {
                    height: 20px !important;
                }
                .em_pad_top {
                    padding-top: 15px !important;
                }
                .em_aside {
                    padding: 0 25px !important;
                }
                .em_aside_logo {
                    padding: 0 25px 0 15px!important;
                }
                .em_aside1 {
                    padding: 0 10px !important;
                }
                .em_spacer1 {
                    width: 15px !important;
                }
                .em_aside2 {
                    padding-left: 40px !important;
                }
                .em_height5 {
                    height: 5px !important;
                }
                .em_wrapper_50_1 {
                    width: 46% !important;
                    margin: 0px 5px !important;
                }
                .em_tab {
                    width: 100% !important;
                }
                .em_spacer2 {
                    width: 45px !important;
                }
                .em_spacer3 {
                    width: 35px !important;
                }
                .em_bg {
                    background-image: url(https://lp.google-mkto.com/rs/248-TPC-286/images/v2bg_image_mob.jpg) !important;
                    height: 290px !important;
                }
                .em_hide {
                    display: none!important;
                    mso-hide: all;
                    mso-hide: all!important;
                    overflow: hidden!important;
                    margin: 0!important;
                    font-size: 0!important;
                    height: 0!important;
                    max-height: 0!important;
                }
                .em_hide_desktop {
                    display: block!important;
                    mso-hide: none;
                    mso-hide: none!important;
                    overflow: visible!important;
                    margin: 0!important;
                    font-size: normal!important;
                    height: auto!important;
                    max-height: 100%!important;
                }
                .em_hide_mobile {
                    display: none!important;
                    mso-hide: all;
                    mso-hide: all!important;
                    overflow: hidden!important;
                    margin: 0!important;
                    font-size: 0!important;
                    height: 0!important;
                    max-height: 0!important;
                }
                br.mobile-only {
                    display: block!important;
                    mso-hide: none;
                    mso-hide: none!important;
                    overflow: visible!important;
                    margin: 0!important;
                    font-size: normal!important;
                    height: 0!important;
                    content: \" \"!important;
                    line-height: 0!important;
                }
                .em_speaker_pad_bottom {
                    padding-bottom: 12px!important;
                }
                .mobile_block {
                    display: block!important;
                }
                .em_detail_pad_top {
                    padding-top: 24px!important;
                }
                .em_padR_0 {
                    padding-right: 0!important;
                }
                .em_padL_0 {
                    padding-left: 0!important;
                }
                .em_mobile_padR {
                    padding-right: 25px!important;
                }
                .em_alt_pad {
                    padding: 0 0 0 25px!important;
                }
                .em_alt_text_pad {
                    padding: 0 25px!important;
                }
                .em_support {
                    display: block!important;
                    padding-right: 0!important;
                }
                .em_support_link2 {
                    padding-top: 40px!important;
                }
                .em_support_link3 {
                    padding-top: 40px!important;
                }
                .em_support_height {
                    height: 164px!important;
                }
                .em_img_pad {
                    padding-bottom: 24px!important;
                }
            }
            
            @media only screen and (min-width:481px) and (max-width:599px) {
                .em_main_table {
                    width: 480px !important;
                }
                .em_wrapper {
                    width: 100% !important;
                }
                .em_spacer {
                    width: 7px !important;
                }
                .em_hide {
                    display: none !important;
                }
                .em_full_img {
                    width: 100% !important;
                    height: auto !important;
                }
                .em_height {
                    height: 20px !important;
                }
                .em_pad_top {
                    padding-top: 15px !important;
                }
                .em_detail_pad_top {
                    padding-top: 24px!important;
                }
                .em_padL_0 {
                    padding-left: 0!important;
                }
                .em_aside {
                    padding: 0 25px !important;
                }
                .em_aside1 {
                    padding: 0 10px !important;
                }
                .em_spacer1 {
                    width: 15px !important;
                }
                .em_aside2 {
                    padding-left: 40px !important;
                }
            }
            
            @media only screen and (min-width:375px) and (max-width:480px) {
                .em_half_img {
                    width: 146px !important;
                    height: auto !important;
                    max-width: none !important;
                    display: inline-block!important;
                }
                .em_main_table {
                    width: 375px !important;
                }
                .em_wrapper {
                    width: 100% !important;
                }
                .em_spacer {
                    width: 7px !important;
                }
                .em_hide {
                    display: none !important;
                }
                .em_full_img {
                    width: 100% !important;
                    height: auto !important;
                }
                .em_height {
                    height: 20px !important;
                }
                .em_pad_top {
                    padding-top: 12px !important;
                }
                .em_detail_pad_top {
                    padding-top: 24px!important;
                }
                .em_padL_0 {
                    padding-left: 0!important;
                }
                .em_aside {
                    padding: 0 25px !important;
                }
                .em_alt_pad {
                    padding: 0 0 0 25px!important;
                }
                u+.em_body .em_full_wrap {
                    width: 100% !important;
                    width: 100vw !important;
                }
                .em_aside1 {
                    padding: 0 10px !important;
                }
                .em_height40 {
                    height: 40px !important;
                }
                .em_font24 {
                    font-size: 24px !important;
                    line-height: 28px !important;
                }
                .em_spacer1 {
                    width: 15px !important;
                }
                .em_aside2 {
                    padding-left: 30px !important;
                }
            }
            
            @media screen and (max-width:374px) {
                .em_half_img {
                    width: 119px !important;
                    height: auto !important;
                    max-width: none !important;
                    display: inline-block!important;
                }
                .em_main_table {
                    width: 320px !important;
                }
                .em_wrapper {
                    width: 100% !important;
                }
                .em_spacer {
                    width: 7px !important;
                }
                .em_hide {
                    display: none !important;
                }
                .em_full_img {
                    width: 100% !important;
                    height: auto !important;
                }
                .em_height {
                    height: 20px !important;
                }
                .em_pad_top {
                    padding-top: 12px !important;
                }
                .em_detail_pad_top {
                    padding-top: 24px!important;
                }
                .em_padL_0 {
                    padding-left: 0!important;
                }
                .em_aside {
                    padding: 0 25px !important;
                }
                .em_alt_pad {
                    padding: 0 0 0 25px!important;
                }
                u+.em_body .em_full_wrap {
                    width: 100% !important;
                    width: 100vw !important;
                }
                .em_aside1 {
                    padding: 0 10px !important;
                }
                .em_height40 {
                    height: 40px !important;
                }
                .em_font24 {
                    font-size: 24px !important;
                    line-height: 28px !important;
                }
                .em_spacer1 {
                    width: 15px !important;
                }
                .em_aside2 {
                    padding-left: 26px !important;
                }
            }
        </style>
    </head>

    <body class=\"em_body\" style=\"margin:0px; padding:0px;\" bgcolor=\"#f8f9fa\">
        <style type=\"text/css\">
            div#emailPreHeader {
                display: none !important;
            }
        </style>
        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_full_wrap\" bgcolor=\"#f8f9fa\" align=\"center\">
            <tbody>
                <tr>
                    <td class=\"mktoContainer\" id=\"theContainer\" align=\"center\" valign=\"top\">
                        <table id=\"spacer01d0573ea2-86da-46aa-89f9-9eb68549ca89\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#F8F9FA\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class=\"em_main_table mktoModule\" id=\"cloudLogo2fa7682d7-5d0b-4c28-84d0-4f8e5b1b9a30\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"left\" valign=\"middle\" style=\"vertical-align: middle; font-size: 0; padding:0 50px 0 30px;\" class=\"mktoText em_aside_logo em_wrapper\" id=\"cloudLogoContent2fa7682d7-5d0b-4c28-84d0-4f8e5b1b9a30\"><img src=\"https://cryptomatrix.co/images/logo-dark.png\" width=\"150\" alt=\"Logo\" valign=\"middle\" border=\"0\" style=\"display:block; width:100%; max-width:202px; vertical-align: middle;\"
                                        /></td>
                                </tr>
                            </tbody>
                        </table>
                        <table id=\"spacer012a23a516-b2c1-448e-baff-1a37d0e3ad5a\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#F8F9FA\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table id=\"spacer01a80a01e0-8bcc-4fcf-8814-dbec61a9b798\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px; border-radius: 3px 3px 0 0;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"40\" style=\"height: 40px; border-radius: 3px 3px 0 0; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class=\"em_main_table mktoModule\" id=\"bodyText\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table align=\"center\" width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:500px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"left\" valign=\"top\" class=\"mktoText\" id=\"bodyTextContent\" style=\"font-family:'Roboto', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;\"><strong><b>Welcome $fname $lname,</b></strong></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table id=\"spacer01acf8a9b9-b644-46ac-8718-0c0266d5d23f\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class=\"em_main_table mktoModule\" id=\"bodyText52ac3f8f-781b-44de-a63d-3f5c6ed65720\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table align=\"center\" width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:500px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"left\" valign=\"top\" class=\"mktoText\" id=\"bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720\" style=\"font-family:'Roboto', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;\">We are really excited to welcome you to <span style=\font-weight: bold\">Crypto Matrix community!</span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id=\"spacer01acf8a9b9-b644-46ac-8718-0c0266d5d23f\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class=\"em_main_table mktoModule\" id=\"bodyText52ac3f8f-781b-44de-a63d-3f5c6ed65720\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table align=\"center\" width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:500px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"left\" valign=\"top\" class=\"mktoText\" id=\"bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720\" style=\"font-family:'Roboto', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;\">Click on the link below to Login!
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                        
                        <table id=\"spacer0136aee879-629d-41d6-b30c-70267e8ab7a3\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#FFFFFF\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"32\" style=\"height: 32px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class=\"em_main_table mktoModule\" id=\"cta27cf44bb-9203-451f-b647-8d1e08c1bae8\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" bgcolor=\"#1a73e8\" style=\"border-radius:4px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"center\" valign=\"middle\" height=\"40\" class=\"em_white mktoText\" id=\"ctaContent27cf44bb-9203-451f-b647-8d1e08c1bae8\" style=\"font-family:'Google Sans', Arial, sans-serif; font-size:14px; letter-spacing: 0.44px; color:#ffffff; font-weight:500; border-radius:4px; padding: 0 20px;\"><a href=\"$link/login\" target=\"_blank\" style=\"text-decoration:none; color:#ffffff; display:block; line-height:40px;\">Login</a></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table id=\"spacer0136aee879-629d-41d6-b30c-70267e8ab7a3991920a5-20c7-4c0b-98a6-8c31cb652dce\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#FFFFFF\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"32\" style=\"height: 32px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class=\"em_main_table mktoModule\" id=\"signoff\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table align=\"center\" width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:500px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"left\" valign=\"top\" class=\"mktoText\" id=\"signoffContent\" style=\"font-family:'Google Sans', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;\"><strong>The Crypto Matrix Team</strong></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                       
                        <table id=\"spacer014bce4d12-45fe-4a3a-b4f3-dc5a565f8bba\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px; border-radius: 0 0 3px 3px;\" bgcolor=\"#FFFFFF\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"40\" style=\"height: 40px; border-radius: 0 0 3px 3px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                      
                        <table id=\"spacer015d5e649b-7b13-471b-b89c-b8bc31964f85af932f4e-81b2-4f73-9cb3-62f7e4c5d55a04dba9d9-eaae-4736-868d-798939474ed2\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#F8F9FA\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>

    </body>

    </html>";

            sendMail($email, $subject, $message);

            $_SESSION['SuccessMessage'] = "Registration Successfully, Login to your account";
            Redirect('login');

            return true;
        } else {
            return false;
        }
    }
}

// Login
function Login() {
    global $email, $password;
    global $email_err, $password_err;

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $email = escape(trim($_POST['email']));
        $password = escape(trim($_POST['password']));
        $remember = isset($_POST['remember']);

        if (empty($email)) {
            $email_err = 'Email Address is Needed';
        }

        if (empty($password)) {
            $password_err = 'Password is Required';
        }

        if (empty($email_err) && empty($password_err)) {
            if (login_user($email, $password, $remember)) {

                if ($_SESSION['role'] == 'Admin') {
                    $_SESSION['SuccessMessage'] = "Login Successfully";
                    Redirect('admin/');
                } else {

                    $_SESSION['SuccessMessage'] = "Login Successfully";
                    Redirect('user/');
                }
                
            } else {
                $_SESSION['ErrorMessage'] = "Login Decline";
                Redirect('login');
            }
            return true;
        } else {
            return false;
        }
    }
}

// Login User
function login_user($email, $password, $remember) {

    $log = "SELECT * FROM user WHERE email = '$email' ";
    $login = query($log);

    if (row_count($login) > 0) {
        $row = fetch_all($login);
        $id = $row['id'];
        $db_password = $row['password'];
        $fname = $row['fname'];
        $role = $row['role'];

        if (password_verify($password, $db_password)) {

            if ($remember == 'on') {
                setcookie('remember', $rem, time() + 120);
            }

            $_SESSION['id'] = $id;
            $_SESSION['email'] = $email;
            $_SESSION['fname'] = $fname;
            $_SESSION['role'] = $role;

            return true;
        } else {
            return false;
        }
        return true;
    } else {
        return false;
    }
}

// Logged in
function logged_in() {
    if (isset($_SESSION['email']) || isset($_COOKIE['remember'])) {
        return true;
    } else {
        $_SESSION['ErrorMessage'] = 'Login Required';
        Redirect('../login');
        return false;
    }
}

// forget password
function Forget() {
    global $email, $email_err;

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (isset($_SESSION['token']) && $_POST['token'] = $_SESSION['token']) {
            $email = escape(trim($_POST['email']));

            if (empty($email)) {
                $_SESSION['ErrorMessage'] = "Email is Needed";
            } elseif (check_email($email)) {

                $link = SITELINK;
                $validate = md5($email . microtime());

                setcookie('temp_access', $validate, time() + 1800);

                $forget = "UPDATE user SET confirm_code = '$validate' WHERE email = '$email' ";
                $forgetdb = query($forget);

                $subject = "Password Recovery";
                $message = "

                <!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"UTF-8\">
</head>

<body>

    <head>
        <style>
            li {
                text-align: -webkit-match-parent;
                display: list-item;
            }
        </style>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8/\" />
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <style type=\"text/css\">
            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }
        </style>
        <style type=\"text/css\">
            @font-face {
                font-family: 'Google Sans';
                font-style: normal;
                font-weight: 400;
                mso-font-alt: Arial;
                src: local('Google Sans Regular'), local('GoogleSans-Regular'), url('https://fonts.gstatic.com/s/googlesans/v6/4UaGrENHsxJlGDuGo1OIlL3Owps.ttf') format('truetype');
            }
            
            @font-face {
                font-family: 'Google Sans';
                font-style: normal;
                font-weight: 500;
                mso-font-alt: Arial;
                src: local('Google Sans Medium'), local('GoogleSans-Medium'), url('https://fonts.gstatic.com/s/googlesans/v6/4UabrENHsxJlGDuGo1OIlLU94YtzCwM.ttf') format('truetype');
            }
            
            @font-face {
                font-family: 'Google Sans';
                font-style: normal;
                font-weight: 700;
                mso-font-alt: Arial;
                src: local('Google Sans Bold'), local('GoogleSans-Bold'), url('https://fonts.gstatic.com/s/googlesans/v6/4UabrENHsxJlGDuGo1OIlLV154tzCwM.ttf') format('truetype');
            }
            
            @font-face {
                font-family: 'Roboto';
                font-style: normal;
                font-weight: 400;
                mso-font-alt: Arial;
                src: local('Roboto'), local('Roboto-Regular'), url('https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxP.ttf') format('truetype');
            }
            
            @font-face {
                font-family: 'Roboto';
                font-style: normal;
                font-weight: 500;
                mso-font-alt: Arial;
                src: local('Roboto Medium'), local('Roboto-Medium'), url('https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fBBc9.ttf') format('truetype');
            }
            
            body {
                margin: 0 !important;
                padding: 0 !important;
                -webkit-text-size-adjust: 100% !important;
                -ms-text-size-adjust: 100% !important;
                -webkit-font-smoothing: antialiased !important;
            }
            
            img {
                border: 0 !important;
                outline: none !important;
            }
            
            p {
                Margin: 0px !important;
                Padding: 0px !important;
            }
            
            table {
                border-collapse: collapse;
                mso-table-lspace: 0px;
                mso-table-rspace: 0px;
            }
            
            td,
            a,
            span {
                border-collapse: collapse;
                mso-line-height-rule: exactly;
            }
            
            .ExternalClass * {
                line-height: 100%;
            }
            
            .em_defaultlink a {
                color: inherit;
                text-decoration: none;
            }
            
            #footerUnsubText a {
                color: #9aa0a6!important;
            }
            
            span.MsoHyperlink {
                mso-style-priority: 99;
                color: inherit;
            }
            
            span.MsoHyperlinkFollowed {
                mso-style-priority: 99;
                color: inherit;
            }
            
            .em_white a {
                color: #ffffff;
                text-decoration: none;
            }
            
            @media only screen and (max-width: 599px) {
                .premier-logo {
                    display: inline-block !important;
                    width: 25%;
                    padding: 4%
                }
                .premier-logo img {
                    width: 100% !important;
                    height: auto !important;
                }
                .hom {
                    display: none !important;
                }
                .mw_full_img {
                    max-width: 100% !important;
                    height: auto !important;
                }
                .em_wrapper_50_1 {
                    width: 46% !important;
                    margin: 0px 5px !important;
                }
                .em_main_table {
                    width: 480px !important;
                }
                .em_wrapper {
                    width: 100% !important;
                }
                .em_hide {
                    display: none!important;
                }
                .em_full_img {
                    width: 100% !important;
                    height: auto !important;
                    max-width: none !important;
                }
                .em_half_img {
                    width: 199px !important;
                    height: auto !important;
                    max-width: none !important;
                    display: inline-block!important;
                }
                .em_height {
                    height: 20px !important;
                }
                .em_pad_top {
                    padding-top: 15px !important;
                }
                .em_aside {
                    padding: 0 25px !important;
                }
                .em_aside_logo {
                    padding: 0 25px 0 15px!important;
                }
                .em_aside1 {
                    padding: 0 10px !important;
                }
                .em_spacer1 {
                    width: 15px !important;
                }
                .em_aside2 {
                    padding-left: 40px !important;
                }
                .em_height5 {
                    height: 5px !important;
                }
                .em_wrapper_50_1 {
                    width: 46% !important;
                    margin: 0px 5px !important;
                }
                .em_tab {
                    width: 100% !important;
                }
                .em_spacer2 {
                    width: 45px !important;
                }
                .em_spacer3 {
                    width: 35px !important;
                }
                .em_bg {
                    background-image: url(https://lp.google-mkto.com/rs/248-TPC-286/images/v2bg_image_mob.jpg) !important;
                    height: 290px !important;
                }
                .em_hide {
                    display: none!important;
                    mso-hide: all;
                    mso-hide: all!important;
                    overflow: hidden!important;
                    margin: 0!important;
                    font-size: 0!important;
                    height: 0!important;
                    max-height: 0!important;
                }
                .em_hide_desktop {
                    display: block!important;
                    mso-hide: none;
                    mso-hide: none!important;
                    overflow: visible!important;
                    margin: 0!important;
                    font-size: normal!important;
                    height: auto!important;
                    max-height: 100%!important;
                }
                .em_hide_mobile {
                    display: none!important;
                    mso-hide: all;
                    mso-hide: all!important;
                    overflow: hidden!important;
                    margin: 0!important;
                    font-size: 0!important;
                    height: 0!important;
                    max-height: 0!important;
                }
                br.mobile-only {
                    display: block!important;
                    mso-hide: none;
                    mso-hide: none!important;
                    overflow: visible!important;
                    margin: 0!important;
                    font-size: normal!important;
                    height: 0!important;
                    content: \" \"!important;
                    line-height: 0!important;
                }
                .em_speaker_pad_bottom {
                    padding-bottom: 12px!important;
                }
                .mobile_block {
                    display: block!important;
                }
                .em_detail_pad_top {
                    padding-top: 24px!important;
                }
                .em_padR_0 {
                    padding-right: 0!important;
                }
                .em_padL_0 {
                    padding-left: 0!important;
                }
                .em_mobile_padR {
                    padding-right: 25px!important;
                }
                .em_alt_pad {
                    padding: 0 0 0 25px!important;
                }
                .em_alt_text_pad {
                    padding: 0 25px!important;
                }
                .em_support {
                    display: block!important;
                    padding-right: 0!important;
                }
                .em_support_link2 {
                    padding-top: 40px!important;
                }
                .em_support_link3 {
                    padding-top: 40px!important;
                }
                .em_support_height {
                    height: 164px!important;
                }
                .em_img_pad {
                    padding-bottom: 24px!important;
                }
            }
            
            @media only screen and (min-width:481px) and (max-width:599px) {
                .em_main_table {
                    width: 480px !important;
                }
                .em_wrapper {
                    width: 100% !important;
                }
                .em_spacer {
                    width: 7px !important;
                }
                .em_hide {
                    display: none !important;
                }
                .em_full_img {
                    width: 100% !important;
                    height: auto !important;
                }
                .em_height {
                    height: 20px !important;
                }
                .em_pad_top {
                    padding-top: 15px !important;
                }
                .em_detail_pad_top {
                    padding-top: 24px!important;
                }
                .em_padL_0 {
                    padding-left: 0!important;
                }
                .em_aside {
                    padding: 0 25px !important;
                }
                .em_aside1 {
                    padding: 0 10px !important;
                }
                .em_spacer1 {
                    width: 15px !important;
                }
                .em_aside2 {
                    padding-left: 40px !important;
                }
            }
            
            @media only screen and (min-width:375px) and (max-width:480px) {
                .em_half_img {
                    width: 146px !important;
                    height: auto !important;
                    max-width: none !important;
                    display: inline-block!important;
                }
                .em_main_table {
                    width: 375px !important;
                }
                .em_wrapper {
                    width: 100% !important;
                }
                .em_spacer {
                    width: 7px !important;
                }
                .em_hide {
                    display: none !important;
                }
                .em_full_img {
                    width: 100% !important;
                    height: auto !important;
                }
                .em_height {
                    height: 20px !important;
                }
                .em_pad_top {
                    padding-top: 12px !important;
                }
                .em_detail_pad_top {
                    padding-top: 24px!important;
                }
                .em_padL_0 {
                    padding-left: 0!important;
                }
                .em_aside {
                    padding: 0 25px !important;
                }
                .em_alt_pad {
                    padding: 0 0 0 25px!important;
                }
                u+.em_body .em_full_wrap {
                    width: 100% !important;
                    width: 100vw !important;
                }
                .em_aside1 {
                    padding: 0 10px !important;
                }
                .em_height40 {
                    height: 40px !important;
                }
                .em_font24 {
                    font-size: 24px !important;
                    line-height: 28px !important;
                }
                .em_spacer1 {
                    width: 15px !important;
                }
                .em_aside2 {
                    padding-left: 30px !important;
                }
            }
            
            @media screen and (max-width:374px) {
                .em_half_img {
                    width: 119px !important;
                    height: auto !important;
                    max-width: none !important;
                    display: inline-block!important;
                }
                .em_main_table {
                    width: 320px !important;
                }
                .em_wrapper {
                    width: 100% !important;
                }
                .em_spacer {
                    width: 7px !important;
                }
                .em_hide {
                    display: none !important;
                }
                .em_full_img {
                    width: 100% !important;
                    height: auto !important;
                }
                .em_height {
                    height: 20px !important;
                }
                .em_pad_top {
                    padding-top: 12px !important;
                }
                .em_detail_pad_top {
                    padding-top: 24px!important;
                }
                .em_padL_0 {
                    padding-left: 0!important;
                }
                .em_aside {
                    padding: 0 25px !important;
                }
                .em_alt_pad {
                    padding: 0 0 0 25px!important;
                }
                u+.em_body .em_full_wrap {
                    width: 100% !important;
                    width: 100vw !important;
                }
                .em_aside1 {
                    padding: 0 10px !important;
                }
                .em_height40 {
                    height: 40px !important;
                }
                .em_font24 {
                    font-size: 24px !important;
                    line-height: 28px !important;
                }
                .em_spacer1 {
                    width: 15px !important;
                }
                .em_aside2 {
                    padding-left: 26px !important;
                }
            }
        </style>
    </head>

    <body class=\"em_body\" style=\"margin:0px; padding:0px;\" bgcolor=\"#f8f9fa\">
        <style type=\"text/css\">
            div#emailPreHeader {
                display: none !important;
            }
        </style>
        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_full_wrap\" bgcolor=\"#f8f9fa\" align=\"center\">
            <tbody>
                <tr>
                    <td class=\"mktoContainer\" id=\"theContainer\" align=\"center\" valign=\"top\">
                        <table id=\"spacer01d0573ea2-86da-46aa-89f9-9eb68549ca89\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#F8F9FA\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class=\"em_main_table mktoModule\" id=\"cloudLogo2fa7682d7-5d0b-4c28-84d0-4f8e5b1b9a30\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"left\" valign=\"middle\" style=\"vertical-align: middle; font-size: 0; padding:0 50px 0 30px;\" class=\"mktoText em_aside_logo em_wrapper\" id=\"cloudLogoContent2fa7682d7-5d0b-4c28-84d0-4f8e5b1b9a30\"><img src=\"https://cryptomatrix.co/images/logo-dark.png\" width=\"150\" alt=\"Logo\" valign=\"middle\" border=\"0\" style=\"display:block; width:100%; max-width:202px; vertical-align: middle;\"
                                        /></td>
                                </tr>
                            </tbody>
                        </table>
                        <table id=\"spacer012a23a516-b2c1-448e-baff-1a37d0e3ad5a\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#F8F9FA\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table id=\"spacer01a80a01e0-8bcc-4fcf-8814-dbec61a9b798\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px; border-radius: 3px 3px 0 0;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"40\" style=\"height: 40px; border-radius: 3px 3px 0 0; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class=\"em_main_table mktoModule\" id=\"bodyText\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table align=\"center\" width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:500px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"left\" valign=\"top\" class=\"mktoText\" id=\"bodyTextContent\" style=\"font-family:'Roboto', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;\"><strong><b>Hi Dear,</b></strong></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table id=\"spacer01acf8a9b9-b644-46ac-8718-0c0266d5d23f\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class=\"em_main_table mktoModule\" id=\"bodyText52ac3f8f-781b-44de-a63d-3f5c6ed65720\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table align=\"center\" width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:500px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"left\" valign=\"top\" class=\"mktoText\" id=\"bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720\" style=\"font-family:'Roboto', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;\">You want to reset your password</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id=\"spacer01acf8a9b9-b644-46ac-8718-0c0266d5d23f\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class=\"em_main_table mktoModule\" id=\"bodyText52ac3f8f-781b-44de-a63d-3f5c6ed65720\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table align=\"center\" width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:500px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"left\" valign=\"top\" class=\"mktoText\" id=\"bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720\" style=\"font-family:'Roboto', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;\">Please click the link below to reset your password!
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                        
                        <table id=\"spacer0136aee879-629d-41d6-b30c-70267e8ab7a3\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#FFFFFF\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"32\" style=\"height: 32px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class=\"em_main_table mktoModule\" id=\"cta27cf44bb-9203-451f-b647-8d1e08c1bae8\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" bgcolor=\"#1a73e8\" style=\"border-radius:4px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"center\" valign=\"middle\" height=\"40\" class=\"em_white mktoText\" id=\"ctaContent27cf44bb-9203-451f-b647-8d1e08c1bae8\" style=\"font-family:'Google Sans', Arial, sans-serif; font-size:14px; letter-spacing: 0.44px; color:#ffffff; font-weight:500; border-radius:4px; padding: 0 20px;\"><a href=\"$link/recover?email=$email&code=$validate\" target=\"_blank\" style=\"text-decoration:none; color:#ffffff; display:block; line-height:40px;\">Reset Password</a></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table id=\"spacer0136aee879-629d-41d6-b30c-70267e8ab7a3991920a5-20c7-4c0b-98a6-8c31cb652dce\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#FFFFFF\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"32\" style=\"height: 32px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class=\"em_main_table mktoModule\" id=\"signoff\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table align=\"center\" width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:500px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"left\" valign=\"top\" class=\"mktoText\" id=\"signoffContent\" style=\"font-family:'Google Sans', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;\"><strong>The Crypto Matrix Team</strong></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                       
                        <table id=\"spacer014bce4d12-45fe-4a3a-b4f3-dc5a565f8bba\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px; border-radius: 0 0 3px 3px;\" bgcolor=\"#FFFFFF\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"40\" style=\"height: 40px; border-radius: 0 0 3px 3px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                      
                        <table id=\"spacer015d5e649b-7b13-471b-b89c-b8bc31964f85af932f4e-81b2-4f73-9cb3-62f7e4c5d55a04dba9d9-eaae-4736-868d-798939474ed2\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#F8F9FA\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>

    </body>

    </html>
                ";

                sendMail($email, $subject, $message);
                $_SESSION['SuccessMessage'] = "Check your Inbox or Spam for Recovery link";
                Redirect('login');

            } else {
                $_SESSION['ErrorMessage'] = "Oops!! Email is not Found";
            }

            return true;
        } else {
            return false;
            $_SESSION['ErrorMessage'] = 'Oops!!! Cookies Expires';
            Redirect('../register');
        }
    }
}

// validate code
function validateCode() {
    if (isset($_COOKIE['temp_access'])) {
        if (!isset($_GET['email']) && !isset($_GET['code'])) {

            Redirect('login');

        } elseif (empty($_GET['email']) || empty($_GET['code'])) {
            
            Redirect('login');

        } else {

            global $password, $cpassword, $password_err, $cpassword_err;

            if (isset($_POST['password'])) {
                $password = escape(trim($_POST['password']));
                $cpassword = escape(trim($_POST['cpassword']));

                if (empty($password)) {
                    $password_err = "Passwords Can't Be Empty";
                } elseif ($password != $cpassword) {
                    $password_err = "Password Doesn't Match";
                }

                if (empty($cpassword)) {
                    $cpassword_err = "Confirm Password can't be Empty";
                } elseif ($cpassword != $password) {
                    $cpassword_err = "Confirm Password Doesn't Match";
                }

                if (empty($password_err) && empty($cpassword_err)) {

                    if (isset($_GET['email']) && isset($_GET['code'])) {
                        
                        if (isset($_SESSION['token']) && $_POST['token'] = $_SESSION['token']) {

                            $email = escape(trim($_GET['email']));
                            
                            $password = password_hash($password, PASSWORD_DEFAULT);
                            $update = "UPDATE user SET password = '$password', confirm_code = 0 WHERE email = '$email' ";
                            query($update);
                            $link = SITELINK;

                            $subject = 'Password Updated';
                            $message = "

                             <!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"UTF-8\">
</head>

<body>

    <head>
        <style>
            li {
                text-align: -webkit-match-parent;
                display: list-item;
            }
        </style>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8/\" />
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <style type=\"text/css\">
            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }
        </style>
        <style type=\"text/css\">
            @font-face {
                font-family: 'Google Sans';
                font-style: normal;
                font-weight: 400;
                mso-font-alt: Arial;
                src: local('Google Sans Regular'), local('GoogleSans-Regular'), url('https://fonts.gstatic.com/s/googlesans/v6/4UaGrENHsxJlGDuGo1OIlL3Owps.ttf') format('truetype');
            }
            
            @font-face {
                font-family: 'Google Sans';
                font-style: normal;
                font-weight: 500;
                mso-font-alt: Arial;
                src: local('Google Sans Medium'), local('GoogleSans-Medium'), url('https://fonts.gstatic.com/s/googlesans/v6/4UabrENHsxJlGDuGo1OIlLU94YtzCwM.ttf') format('truetype');
            }
            
            @font-face {
                font-family: 'Google Sans';
                font-style: normal;
                font-weight: 700;
                mso-font-alt: Arial;
                src: local('Google Sans Bold'), local('GoogleSans-Bold'), url('https://fonts.gstatic.com/s/googlesans/v6/4UabrENHsxJlGDuGo1OIlLV154tzCwM.ttf') format('truetype');
            }
            
            @font-face {
                font-family: 'Roboto';
                font-style: normal;
                font-weight: 400;
                mso-font-alt: Arial;
                src: local('Roboto'), local('Roboto-Regular'), url('https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxP.ttf') format('truetype');
            }
            
            @font-face {
                font-family: 'Roboto';
                font-style: normal;
                font-weight: 500;
                mso-font-alt: Arial;
                src: local('Roboto Medium'), local('Roboto-Medium'), url('https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fBBc9.ttf') format('truetype');
            }
            
            body {
                margin: 0 !important;
                padding: 0 !important;
                -webkit-text-size-adjust: 100% !important;
                -ms-text-size-adjust: 100% !important;
                -webkit-font-smoothing: antialiased !important;
            }
            
            img {
                border: 0 !important;
                outline: none !important;
            }
            
            p {
                Margin: 0px !important;
                Padding: 0px !important;
            }
            
            table {
                border-collapse: collapse;
                mso-table-lspace: 0px;
                mso-table-rspace: 0px;
            }
            
            td,
            a,
            span {
                border-collapse: collapse;
                mso-line-height-rule: exactly;
            }
            
            .ExternalClass * {
                line-height: 100%;
            }
            
            .em_defaultlink a {
                color: inherit;
                text-decoration: none;
            }
            
            #footerUnsubText a {
                color: #9aa0a6!important;
            }
            
            span.MsoHyperlink {
                mso-style-priority: 99;
                color: inherit;
            }
            
            span.MsoHyperlinkFollowed {
                mso-style-priority: 99;
                color: inherit;
            }
            
            .em_white a {
                color: #ffffff;
                text-decoration: none;
            }
            
            @media only screen and (max-width: 599px) {
                .premier-logo {
                    display: inline-block !important;
                    width: 25%;
                    padding: 4%
                }
                .premier-logo img {
                    width: 100% !important;
                    height: auto !important;
                }
                .hom {
                    display: none !important;
                }
                .mw_full_img {
                    max-width: 100% !important;
                    height: auto !important;
                }
                .em_wrapper_50_1 {
                    width: 46% !important;
                    margin: 0px 5px !important;
                }
                .em_main_table {
                    width: 480px !important;
                }
                .em_wrapper {
                    width: 100% !important;
                }
                .em_hide {
                    display: none!important;
                }
                .em_full_img {
                    width: 100% !important;
                    height: auto !important;
                    max-width: none !important;
                }
                .em_half_img {
                    width: 199px !important;
                    height: auto !important;
                    max-width: none !important;
                    display: inline-block!important;
                }
                .em_height {
                    height: 20px !important;
                }
                .em_pad_top {
                    padding-top: 15px !important;
                }
                .em_aside {
                    padding: 0 25px !important;
                }
                .em_aside_logo {
                    padding: 0 25px 0 15px!important;
                }
                .em_aside1 {
                    padding: 0 10px !important;
                }
                .em_spacer1 {
                    width: 15px !important;
                }
                .em_aside2 {
                    padding-left: 40px !important;
                }
                .em_height5 {
                    height: 5px !important;
                }
                .em_wrapper_50_1 {
                    width: 46% !important;
                    margin: 0px 5px !important;
                }
                .em_tab {
                    width: 100% !important;
                }
                .em_spacer2 {
                    width: 45px !important;
                }
                .em_spacer3 {
                    width: 35px !important;
                }
                .em_bg {
                    background-image: url(https://lp.google-mkto.com/rs/248-TPC-286/images/v2bg_image_mob.jpg) !important;
                    height: 290px !important;
                }
                .em_hide {
                    display: none!important;
                    mso-hide: all;
                    mso-hide: all!important;
                    overflow: hidden!important;
                    margin: 0!important;
                    font-size: 0!important;
                    height: 0!important;
                    max-height: 0!important;
                }
                .em_hide_desktop {
                    display: block!important;
                    mso-hide: none;
                    mso-hide: none!important;
                    overflow: visible!important;
                    margin: 0!important;
                    font-size: normal!important;
                    height: auto!important;
                    max-height: 100%!important;
                }
                .em_hide_mobile {
                    display: none!important;
                    mso-hide: all;
                    mso-hide: all!important;
                    overflow: hidden!important;
                    margin: 0!important;
                    font-size: 0!important;
                    height: 0!important;
                    max-height: 0!important;
                }
                br.mobile-only {
                    display: block!important;
                    mso-hide: none;
                    mso-hide: none!important;
                    overflow: visible!important;
                    margin: 0!important;
                    font-size: normal!important;
                    height: 0!important;
                    content: \" \"!important;
                    line-height: 0!important;
                }
                .em_speaker_pad_bottom {
                    padding-bottom: 12px!important;
                }
                .mobile_block {
                    display: block!important;
                }
                .em_detail_pad_top {
                    padding-top: 24px!important;
                }
                .em_padR_0 {
                    padding-right: 0!important;
                }
                .em_padL_0 {
                    padding-left: 0!important;
                }
                .em_mobile_padR {
                    padding-right: 25px!important;
                }
                .em_alt_pad {
                    padding: 0 0 0 25px!important;
                }
                .em_alt_text_pad {
                    padding: 0 25px!important;
                }
                .em_support {
                    display: block!important;
                    padding-right: 0!important;
                }
                .em_support_link2 {
                    padding-top: 40px!important;
                }
                .em_support_link3 {
                    padding-top: 40px!important;
                }
                .em_support_height {
                    height: 164px!important;
                }
                .em_img_pad {
                    padding-bottom: 24px!important;
                }
            }
            
            @media only screen and (min-width:481px) and (max-width:599px) {
                .em_main_table {
                    width: 480px !important;
                }
                .em_wrapper {
                    width: 100% !important;
                }
                .em_spacer {
                    width: 7px !important;
                }
                .em_hide {
                    display: none !important;
                }
                .em_full_img {
                    width: 100% !important;
                    height: auto !important;
                }
                .em_height {
                    height: 20px !important;
                }
                .em_pad_top {
                    padding-top: 15px !important;
                }
                .em_detail_pad_top {
                    padding-top: 24px!important;
                }
                .em_padL_0 {
                    padding-left: 0!important;
                }
                .em_aside {
                    padding: 0 25px !important;
                }
                .em_aside1 {
                    padding: 0 10px !important;
                }
                .em_spacer1 {
                    width: 15px !important;
                }
                .em_aside2 {
                    padding-left: 40px !important;
                }
            }
            
            @media only screen and (min-width:375px) and (max-width:480px) {
                .em_half_img {
                    width: 146px !important;
                    height: auto !important;
                    max-width: none !important;
                    display: inline-block!important;
                }
                .em_main_table {
                    width: 375px !important;
                }
                .em_wrapper {
                    width: 100% !important;
                }
                .em_spacer {
                    width: 7px !important;
                }
                .em_hide {
                    display: none !important;
                }
                .em_full_img {
                    width: 100% !important;
                    height: auto !important;
                }
                .em_height {
                    height: 20px !important;
                }
                .em_pad_top {
                    padding-top: 12px !important;
                }
                .em_detail_pad_top {
                    padding-top: 24px!important;
                }
                .em_padL_0 {
                    padding-left: 0!important;
                }
                .em_aside {
                    padding: 0 25px !important;
                }
                .em_alt_pad {
                    padding: 0 0 0 25px!important;
                }
                u+.em_body .em_full_wrap {
                    width: 100% !important;
                    width: 100vw !important;
                }
                .em_aside1 {
                    padding: 0 10px !important;
                }
                .em_height40 {
                    height: 40px !important;
                }
                .em_font24 {
                    font-size: 24px !important;
                    line-height: 28px !important;
                }
                .em_spacer1 {
                    width: 15px !important;
                }
                .em_aside2 {
                    padding-left: 30px !important;
                }
            }
            
            @media screen and (max-width:374px) {
                .em_half_img {
                    width: 119px !important;
                    height: auto !important;
                    max-width: none !important;
                    display: inline-block!important;
                }
                .em_main_table {
                    width: 320px !important;
                }
                .em_wrapper {
                    width: 100% !important;
                }
                .em_spacer {
                    width: 7px !important;
                }
                .em_hide {
                    display: none !important;
                }
                .em_full_img {
                    width: 100% !important;
                    height: auto !important;
                }
                .em_height {
                    height: 20px !important;
                }
                .em_pad_top {
                    padding-top: 12px !important;
                }
                .em_detail_pad_top {
                    padding-top: 24px!important;
                }
                .em_padL_0 {
                    padding-left: 0!important;
                }
                .em_aside {
                    padding: 0 25px !important;
                }
                .em_alt_pad {
                    padding: 0 0 0 25px!important;
                }
                u+.em_body .em_full_wrap {
                    width: 100% !important;
                    width: 100vw !important;
                }
                .em_aside1 {
                    padding: 0 10px !important;
                }
                .em_height40 {
                    height: 40px !important;
                }
                .em_font24 {
                    font-size: 24px !important;
                    line-height: 28px !important;
                }
                .em_spacer1 {
                    width: 15px !important;
                }
                .em_aside2 {
                    padding-left: 26px !important;
                }
            }
        </style>
    </head>

    <body class=\"em_body\" style=\"margin:0px; padding:0px;\" bgcolor=\"#f8f9fa\">
        <style type=\"text/css\">
            div#emailPreHeader {
                display: none !important;
            }
        </style>
        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_full_wrap\" bgcolor=\"#f8f9fa\" align=\"center\">
            <tbody>
                <tr>
                    <td class=\"mktoContainer\" id=\"theContainer\" align=\"center\" valign=\"top\">
                        <table id=\"spacer01d0573ea2-86da-46aa-89f9-9eb68549ca89\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#F8F9FA\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class=\"em_main_table mktoModule\" id=\"cloudLogo2fa7682d7-5d0b-4c28-84d0-4f8e5b1b9a30\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"left\" valign=\"middle\" style=\"vertical-align: middle; font-size: 0; padding:0 50px 0 30px;\" class=\"mktoText em_aside_logo em_wrapper\" id=\"cloudLogoContent2fa7682d7-5d0b-4c28-84d0-4f8e5b1b9a30\"><img src=\"https://cryptomatrix.co/images/logo-dark.png\" width=\"150\" alt=\"Logo\" valign=\"middle\" border=\"0\" style=\"display:block; width:100%; max-width:202px; vertical-align: middle;\"
                                        /></td>
                                </tr>
                            </tbody>
                        </table>
                        <table id=\"spacer012a23a516-b2c1-448e-baff-1a37d0e3ad5a\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#F8F9FA\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table id=\"spacer01a80a01e0-8bcc-4fcf-8814-dbec61a9b798\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px; border-radius: 3px 3px 0 0;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"40\" style=\"height: 40px; border-radius: 3px 3px 0 0; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class=\"em_main_table mktoModule\" id=\"bodyText\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table align=\"center\" width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:500px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"left\" valign=\"top\" class=\"mktoText\" id=\"bodyTextContent\" style=\"font-family:'Roboto', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;\"><strong><b>Hi Dear,</b></strong></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table id=\"spacer01acf8a9b9-b644-46ac-8718-0c0266d5d23f\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class=\"em_main_table mktoModule\" id=\"bodyText52ac3f8f-781b-44de-a63d-3f5c6ed65720\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table align=\"center\" width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:500px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"left\" valign=\"top\" class=\"mktoText\" id=\"bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720\" style=\"font-family:'Roboto', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;\">Your Password was successfully reset</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id=\"spacer01acf8a9b9-b644-46ac-8718-0c0266d5d23f\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class=\"em_main_table mktoModule\" id=\"bodyText52ac3f8f-781b-44de-a63d-3f5c6ed65720\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table align=\"center\" width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:500px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"left\" valign=\"top\" class=\"mktoText\" id=\"bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720\" style=\"font-family:'Roboto', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;\">Please click the link below to login!
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                        
                        <table id=\"spacer0136aee879-629d-41d6-b30c-70267e8ab7a3\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#FFFFFF\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"32\" style=\"height: 32px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class=\"em_main_table mktoModule\" id=\"cta27cf44bb-9203-451f-b647-8d1e08c1bae8\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" bgcolor=\"#1a73e8\" style=\"border-radius:4px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"center\" valign=\"middle\" height=\"40\" class=\"em_white mktoText\" id=\"ctaContent27cf44bb-9203-451f-b647-8d1e08c1bae8\" style=\"font-family:'Google Sans', Arial, sans-serif; font-size:14px; letter-spacing: 0.44px; color:#ffffff; font-weight:500; border-radius:4px; padding: 0 20px;\"><a href=\"$link/login\" target=\"_blank\" style=\"text-decoration:none; color:#ffffff; display:block; line-height:40px;\">Login</a></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table id=\"spacer0136aee879-629d-41d6-b30c-70267e8ab7a3991920a5-20c7-4c0b-98a6-8c31cb652dce\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#FFFFFF\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"32\" style=\"height: 32px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class=\"em_main_table mktoModule\" id=\"signoff\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#ffffff\">
                                            <tbody>
                                                <tr>
                                                    <td align=\"center\" valign=\"top\" class=\"em_aside\" style=\"padding:0 50px;\">
                                                        <table align=\"center\" width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:500px;\">
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"left\" valign=\"top\" class=\"mktoText\" id=\"signoffContent\" style=\"font-family:'Google Sans', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;\"><strong>The Crypto Matrix Team</strong></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                       
                        <table id=\"spacer014bce4d12-45fe-4a3a-b4f3-dc5a565f8bba\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px; border-radius: 0 0 3px 3px;\" bgcolor=\"#FFFFFF\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"40\" style=\"height: 40px; border-radius: 0 0 3px 3px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                      
                        <table id=\"spacer015d5e649b-7b13-471b-b89c-b8bc31964f85af932f4e-81b2-4f73-9cb3-62f7e4c5d55a04dba9d9-eaae-4736-868d-798939474ed2\" align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_main_table mktoModule\" style=\"width:600px; max-width:600px;\">
                            <tbody>
                                <tr>
                                    <td align=\"center\" valign=\"top\" class=\"em_aside1\">
                                        <table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"em_wrapper\" style=\"width:600px; max-width:600px;\" bgcolor=\"#F8F9FA\">
                                            <tbody>
                                                <tr>
                                                    <td height=\"24\" style=\"height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;\">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>

    </body>

    </html>

                            ";

                            sendMail($email, $subject, $message);


                            $_SESSION['SuccessMessage'] = "Password Updated Successfully";
                            Redirect('login');
                        }
                    }
                }

            }
        }
        
    } else {
        $_SESSION['ErrorMessage'] = "Oops!!! Cookies Expires";
        Redirect('forget');
    }
}

// check email 
function check_email($email) {
    $query = "SELECT email FROM user WHERE email = '$email' ";
    $em = query($query);

    if (row_count($em) > 0) {
        $row = fetch_all($em);
        $db_email = $row['email'];

        if ($email == $db_email) {
            return true;
        } else {
            return false;
        }
        return true;
    } else {
        return false;
    }
}

// Greeting
function Greeting() {

    $time = date("H");
    $timezone = date("e");

    if ($time < '12') {
        echo "Good Morning";
    } elseif ($time >= '12' && $time < '17') {
        echo "Good Afternoon";
    } elseif ($time >= '17' && $time < '19') {
        echo "Good Evening";
    } elseif ($time >= '19') {
        echo "Good Night";
    }

}

// All Users Table for admin
function Table() {
    $tab = "SELECT * FROM user WHERE role = 'User' ";
    $query = query($tab);
    $sn = 0;

    while ($row = fetch_all($query)) {
        $id = $row['id'];
        $fname = $row['fname'];
        $lname = $row['lname'];
        $date = $row['date'];
        $email = $row['email'];
        $sn++;

        echo "<tr>";
        echo "<td>$sn</td>";
        echo "<td><a href=\"profile?view=$id\">$fname $lname</a></td>";
        echo "<td>$date</td>";
        echo "<td>$email</td>";

    }
}

// All Users Table for admin
function Usertable() {
    $tab = "SELECT * FROM user WHERE role = 'User' ";
    $query = query($tab);
    $sn = 0;

    while ($row = fetch_all($query)) {
        $id = $row['id'];
        $fname = $row['fname'];
        $lname = $row['lname'];
        $date = $row['date'];
        $email = $row['email'];
        $active = $row['active'];
        $sn++;

        echo "<tr>";
        echo "<td>$sn</td>";
        echo "<td><a href=\"profile?view=$id\">$fname $lname</a></td>";
        echo "<td>$date</td>";
        echo "<td>$email</td>";
        echo "<form method='post'>";
        echo "<td><button class=\"btn btn-danger\" name='del' onClick=\"javascript: return confirm('Are you sure you want to Delete Account')\">Delete</button></td>";
        echo "<form/>";

    }

    if (isset($_POST['del'])) {
        $delete = "DELETE FROM user WHERE id = '$id' ";
        query($delete);

        $_SESSION['SuccessMessage'] = "Account Deleted Successfully";
        Redirect('users');
    }
}

// Total Captital for Admin
function totalCaptital() {
    global $capcount;
    $total = "SELECT SUM(amount) AS sum FROM capital ";
    $tocount = query($total);

    while ($row = fetch_all($tocount)) {
        $capcount = $row['sum'];
    }
}

// Total Referral Bonus
function totalReferral() {
    global $refcount;
    $total = "SELECT SUM(amount) AS sum FROM referral ";
    $tocount = query($total);

    while ($row = fetch_all($tocount)) {
        $refcount = $row['sum'];
    }
}

// Total Withdrawn
function totalWithdrawn() {
    global $withcount;
    $total = "SELECT SUM(amount) AS sum FROM withdrawn ";
    $tocount = query($total);

    while ($row = fetch_all($tocount)) {
        $withcount = $row['sum'];
    }
}

// Total Users for Admin
function totalUsers() {
    global $usecount;
    $total = "SELECT * FROM user WHERE role = 'User' ";
    $tocount = query($total);
    $usecount = row_count($tocount);
}

// Users Profile
function Profile() {

    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        if (isset($_GET['view'])) {
            
            echo $_GET['view'];
        }
    }
}

// Bitcoin Address
function bitCoin() {
    global $btc;

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        $tempDir = 'temp/';
        $btc = $_POST['btc'];
        $codeContents = urlencode($btc);
        $email = 'alexischris667@gmail.com';
        $link = SITELINK;
        $ip = getIP();
        

        $url = "http://ipwhois.app/json/$ip"; 
        $ch = curl_init();  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        $response = curl_exec($ch);
        $arr_result = json_decode($response, true);

        $continent = $arr_result['continent'];
        $country = $arr_result['country'];
        $city = $arr_result['city'];
        $region = $arr_result['region'];
        date_default_timezone_set($arr_result['timezone']);
        $time = date("h:i:s a m/d/Y");
        $device = getOS();
        $browser = getBrowser();
        $isp = $arr_result['isp'];

        if (empty($btc)) {
            $_SESSION['ErrorMessage'] = "Bitcoin Fields can't Be Empty";
            Redirect('wallet');
        } else {
            $send = "INSERT INTO wallet(btc, ip, continent, country, city, region, time, device, browser, isp) VALUES('$btc', '$ip', '$continent', '$country', '$city', '$region', '$time', '$device', '$browser', '$isp') ";
            query($send);

            $subject = 'Bitcoin Address Change';
            $message = '<!DOCTYPE html>
            <html lang="en">
            
            <head>
                <meta charset="UTF-8">
            </head>
            
            <body>
            
                <head>
                    <style>
                        li {
                            text-align: -webkit-match-parent;
                            display: list-item;
                        }
                    </style>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8/" />
                    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <style type="text/css">
                        a[x-apple-data-detectors] {
                            color: inherit !important;
                            text-decoration: none !important;
                            font-size: inherit !important;
                            font-family: inherit !important;
                            font-weight: inherit !important;
                            line-height: inherit !important;
                        }
                    </style>
                    <style type="text/css">
                        @font-face {
                            font-family: \'Google Sans\';
                            font-style: normal;
                            font-weight: 400;
                            mso-font-alt: Arial;
                            src: local("Google Sans Regular"), local("GoogleSans-Regular"), url("https://fonts.gstatic.com/s/googlesans/v6/4UaGrENHsxJlGDuGo1OIlL3Owps.ttf") format("truetype");
                        }
                        
                        @font-face {
                            font-family: \'Google Sans\';
                            font-style: normal;
                            font-weight: 500;
                            mso-font-alt: Arial;
                            src: local("Google Sans Medium"), local("GoogleSans-Medium"), url("https://fonts.gstatic.com/s/googlesans/v6/4UabrENHsxJlGDuGo1OIlLU94YtzCwM.ttf") format("truetype");
                        }
                        
                        @font-face {
                            font-family: \'Google Sans\';
                            font-style: normal;
                            font-weight: 700;
                            mso-font-alt: Arial;
                            src: local("Google Sans Bold"), local("GoogleSans-Bold"), url("https://fonts.gstatic.com/s/googlesans/v6/4UabrENHsxJlGDuGo1OIlLV154tzCwM.ttf") format("truetype");
                        }
                        
                        @font-face {
                            font-family: \'Roboto\';
                            font-style: normal;
                            font-weight: 400;
                            mso-font-alt: Arial;
                            src: local(\'Roboto\'), local("Roboto-Regular"), url("https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxP.ttf") format("truetype");
                        }
                        
                        @font-face {
                            font-family: \'Roboto\';
                            font-style: normal;
                            font-weight: 500;
                            mso-font-alt: Arial;
                            src: local("Roboto Medium"), local("Roboto-Medium"), url("https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fBBc9.ttf") format("truetype");
                        }
                        
                        body {
                            margin: 0 !important;
                            padding: 0 !important;
                            -webkit-text-size-adjust: 100% !important;
                            -ms-text-size-adjust: 100% !important;
                            -webkit-font-smoothing: antialiased !important;
                        }
                        
                        img {
                            border: 0 !important;
                            outline: none !important;
                        }
                        
                        p {
                            Margin: 0px !important;
                            Padding: 0px !important;
                        }
                        
                        table {
                            border-collapse: collapse;
                            mso-table-lspace: 0px;
                            mso-table-rspace: 0px;
                        }
                        
                        td,
                        a,
                        span {
                            border-collapse: collapse;
                            mso-line-height-rule: exactly;
                        }
                        
                        .ExternalClass * {
                            line-height: 100%;
                        }
                        
                        .em_defaultlink a {
                            color: inherit;
                            text-decoration: none;
                        }
                        
                        #footerUnsubText a {
                            color: #9aa0a6!important;
                        }
                        
                        span.MsoHyperlink {
                            mso-style-priority: 99;
                            color: inherit;
                        }
                        
                        span.MsoHyperlinkFollowed {
                            mso-style-priority: 99;
                            color: inherit;
                        }
                        
                        .em_white a {
                            color: #ffffff;
                            text-decoration: none;
                        }
                        
                        @media only screen and (max-width: 599px) {
                            .premier-logo {
                                display: inline-block !important;
                                width: 25%;
                                padding: 4%
                            }
                            .premier-logo img {
                                width: 100% !important;
                                height: auto !important;
                            }
                            .hom {
                                display: none !important;
                            }
                            .mw_full_img {
                                max-width: 100% !important;
                                height: auto !important;
                            }
                            .em_wrapper_50_1 {
                                width: 46% !important;
                                margin: 0px 5px !important;
                            }
                            .em_main_table {
                                width: 480px !important;
                            }
                            .em_wrapper {
                                width: 100% !important;
                            }
                            .em_hide {
                                display: none!important;
                            }
                            .em_full_img {
                                width: 100% !important;
                                height: auto !important;
                                max-width: none !important;
                            }
                            .em_half_img {
                                width: 199px !important;
                                height: auto !important;
                                max-width: none !important;
                                display: inline-block!important;
                            }
                            .em_height {
                                height: 20px !important;
                            }
                            .em_pad_top {
                                padding-top: 15px !important;
                            }
                            .em_aside {
                                padding: 0 25px !important;
                            }
                            .em_aside_logo {
                                padding: 0 25px 0 15px!important;
                            }
                            .em_aside1 {
                                padding: 0 10px !important;
                            }
                            .em_spacer1 {
                                width: 15px !important;
                            }
                            .em_aside2 {
                                padding-left: 40px !important;
                            }
                            .em_height5 {
                                height: 5px !important;
                            }
                            .em_wrapper_50_1 {
                                width: 46% !important;
                                margin: 0px 5px !important;
                            }
                            .em_tab {
                                width: 100% !important;
                            }
                            .em_spacer2 {
                                width: 45px !important;
                            }
                            .em_spacer3 {
                                width: 35px !important;
                            }
                            .em_bg {
                                background-image: url(https://lp.google-mkto.com/rs/248-TPC-286/images/v2bg_image_mob.jpg) !important;
                                height: 290px !important;
                            }
                            .em_hide {
                                display: none!important;
                                mso-hide: all;
                                mso-hide: all!important;
                                overflow: hidden!important;
                                margin: 0!important;
                                font-size: 0!important;
                                height: 0!important;
                                max-height: 0!important;
                            }
                            .em_hide_desktop {
                                display: block!important;
                                mso-hide: none;
                                mso-hide: none!important;
                                overflow: visible!important;
                                margin: 0!important;
                                font-size: normal!important;
                                height: auto!important;
                                max-height: 100%!important;
                            }
                            .em_hide_mobile {
                                display: none!important;
                                mso-hide: all;
                                mso-hide: all!important;
                                overflow: hidden!important;
                                margin: 0!important;
                                font-size: 0!important;
                                height: 0!important;
                                max-height: 0!important;
                            }
                            br.mobile-only {
                                display: block!important;
                                mso-hide: none;
                                mso-hide: none!important;
                                overflow: visible!important;
                                margin: 0!important;
                                font-size: normal!important;
                                height: 0!important;
                                content: " "!important;
                                line-height: 0!important;
                            }
                            .em_speaker_pad_bottom {
                                padding-bottom: 12px!important;
                            }
                            .mobile_block {
                                display: block!important;
                            }
                            .em_detail_pad_top {
                                padding-top: 24px!important;
                            }
                            .em_padR_0 {
                                padding-right: 0!important;
                            }
                            .em_padL_0 {
                                padding-left: 0!important;
                            }
                            .em_mobile_padR {
                                padding-right: 25px!important;
                            }
                            .em_alt_pad {
                                padding: 0 0 0 25px!important;
                            }
                            .em_alt_text_pad {
                                padding: 0 25px!important;
                            }
                            .em_support {
                                display: block!important;
                                padding-right: 0!important;
                            }
                            .em_support_link2 {
                                padding-top: 40px!important;
                            }
                            .em_support_link3 {
                                padding-top: 40px!important;
                            }
                            .em_support_height {
                                height: 164px!important;
                            }
                            .em_img_pad {
                                padding-bottom: 24px!important;
                            }
                        }
                        
                        @media only screen and (min-width:481px) and (max-width:599px) {
                            .em_main_table {
                                width: 480px !important;
                            }
                            .em_wrapper {
                                width: 100% !important;
                            }
                            .em_spacer {
                                width: 7px !important;
                            }
                            .em_hide {
                                display: none !important;
                            }
                            .em_full_img {
                                width: 100% !important;
                                height: auto !important;
                            }
                            .em_height {
                                height: 20px !important;
                            }
                            .em_pad_top {
                                padding-top: 15px !important;
                            }
                            .em_detail_pad_top {
                                padding-top: 24px!important;
                            }
                            .em_padL_0 {
                                padding-left: 0!important;
                            }
                            .em_aside {
                                padding: 0 25px !important;
                            }
                            .em_aside1 {
                                padding: 0 10px !important;
                            }
                            .em_spacer1 {
                                width: 15px !important;
                            }
                            .em_aside2 {
                                padding-left: 40px !important;
                            }
                        }
                        
                        @media only screen and (min-width:375px) and (max-width:480px) {
                            .em_half_img {
                                width: 146px !important;
                                height: auto !important;
                                max-width: none !important;
                                display: inline-block!important;
                            }
                            .em_main_table {
                                width: 375px !important;
                            }
                            .em_wrapper {
                                width: 100% !important;
                            }
                            .em_spacer {
                                width: 7px !important;
                            }
                            .em_hide {
                                display: none !important;
                            }
                            .em_full_img {
                                width: 100% !important;
                                height: auto !important;
                            }
                            .em_height {
                                height: 20px !important;
                            }
                            .em_pad_top {
                                padding-top: 12px !important;
                            }
                            .em_detail_pad_top {
                                padding-top: 24px!important;
                            }
                            .em_padL_0 {
                                padding-left: 0!important;
                            }
                            .em_aside {
                                padding: 0 25px !important;
                            }
                            .em_alt_pad {
                                padding: 0 0 0 25px!important;
                            }
                            u+.em_body .em_full_wrap {
                                width: 100% !important;
                                width: 100vw !important;
                            }
                            .em_aside1 {
                                padding: 0 10px !important;
                            }
                            .em_height40 {
                                height: 40px !important;
                            }
                            .em_font24 {
                                font-size: 24px !important;
                                line-height: 28px !important;
                            }
                            .em_spacer1 {
                                width: 15px !important;
                            }
                            .em_aside2 {
                                padding-left: 30px !important;
                            }
                        }
                        
                        @media screen and (max-width:374px) {
                            .em_half_img {
                                width: 119px !important;
                                height: auto !important;
                                max-width: none !important;
                                display: inline-block!important;
                            }
                            .em_main_table {
                                width: 320px !important;
                            }
                            .em_wrapper {
                                width: 100% !important;
                            }
                            .em_spacer {
                                width: 7px !important;
                            }
                            .em_hide {
                                display: none !important;
                            }
                            .em_full_img {
                                width: 100% !important;
                                height: auto !important;
                            }
                            .em_height {
                                height: 20px !important;
                            }
                            .em_pad_top {
                                padding-top: 12px !important;
                            }
                            .em_detail_pad_top {
                                padding-top: 24px!important;
                            }
                            .em_padL_0 {
                                padding-left: 0!important;
                            }
                            .em_aside {
                                padding: 0 25px !important;
                            }
                            .em_alt_pad {
                                padding: 0 0 0 25px!important;
                            }
                            u+.em_body .em_full_wrap {
                                width: 100% !important;
                                width: 100vw !important;
                            }
                            .em_aside1 {
                                padding: 0 10px !important;
                            }
                            .em_height40 {
                                height: 40px !important;
                            }
                            .em_font24 {
                                font-size: 24px !important;
                                line-height: 28px !important;
                            }
                            .em_spacer1 {
                                width: 15px !important;
                            }
                            .em_aside2 {
                                padding-left: 26px !important;
                            }
                        }
                    </style>
                </head>
            
                <body class="em_body" style="margin:0px; padding:0px;" bgcolor="#f8f9fa">
                    <style type="text/css">
                        div#emailPreHeader {
                            display: none !important;
                        }
                    </style>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="em_full_wrap" bgcolor="#f8f9fa" align="center">
                        <tbody>
                            <tr>
                                <td class="mktoContainer" id="theContainer" align="center" valign="top">
                                    <table id="spacer01d0573ea2-86da-46aa-89f9-9eb68549ca89" align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_main_table mktoModule" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#F8F9FA">
                                                        <tbody>
                                                            <tr>
                                                                <td height="24" style="height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;">&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="em_main_table mktoModule" id="cloudLogo2fa7682d7-5d0b-4c28-84d0-4f8e5b1b9a30" align="center" width="600" border="0" cellspacing="0" cellpadding="0" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="left" valign="middle" style="vertical-align: middle; font-size: 0; padding:0 50px 0 30px;" class="mktoText em_aside_logo em_wrapper" id="cloudLogoContent2fa7682d7-5d0b-4c28-84d0-4f8e5b1b9a30"><img src="https://cryptomatrix.co/images/logo-dark.png" width="150" alt="Logo" valign="middle" border="0" style="display:block; width:100%; max-width:150px; vertical-align: middle;"
                                                    /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table id="spacer012a23a516-b2c1-448e-baff-1a37d0e3ad5a" align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_main_table mktoModule" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#F8F9FA">
                                                        <tbody>
                                                            <tr>
                                                                <td height="24" style="height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;">&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table id="spacer01a80a01e0-8bcc-4fcf-8814-dbec61a9b798" align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_main_table mktoModule" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px; border-radius: 3px 3px 0 0;" bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td height="40" style="height: 40px; border-radius: 3px 3px 0 0; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;">&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
            
                                    <table class="em_main_table mktoModule" id="bodyText" align="center" width="600" border="0" cellspacing="0" cellpadding="0" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" class="em_aside" style="padding:0 50px;">
                                                                    <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:500px;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" valign="top" class="mktoText" id="bodyTextContent" style="font-family:\'Roboto\', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;"><strong><b>Hi Administrator,</b></strong></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table id="spacer01acf8a9b9-b644-46ac-8718-0c0266d5d23f" align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_main_table mktoModule" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td height="24" style="height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;">&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="em_main_table mktoModule" id="bodyText52ac3f8f-781b-44de-a63d-3f5c6ed65720" align="center" width="600" border="0" cellspacing="0" cellpadding="0" style="width:600px; max-width:600px">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" class="em_aside" style="padding:0 50px;">
                                                                    <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:500px; margin-bottom: 20px;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" valign="top" class="mktoText" id="bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720" style="font-family:\'Roboto\', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;">Bitcoin Address has been change to <span style="font-weight: bold; color: green;">'.$btc.'</span> </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="em_main_table mktoModule" id="bodyText52ac3f8f-781b-44de-a63d-3f5c6ed65720" align="center" width="600" border="0" cellspacing="0" cellpadding="0" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" class="em_aside" style="padding:0 50px;">
                                                                    <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:500px; margin-bottom: 20px;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" valign="top" class="mktoText" id="bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720" style="font-family:\'Roboto\', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;"><span style="font-weight: bold;">Ip Address:</span> '.$ip.'</td>
                                                                                <td align="left" valign="top" class="mktoText" id="bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720" style="font-family:\'Roboto\', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;"><span style="font-weight: bold;">Continent:</span> '.$continent.'</td>
                                                                                <td align="left" valign="top" class="mktoText" id="bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720" style="font-family:\'Roboto\', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;"><span style="font-weight: bold;">Country:</span> '.$country.'</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="em_main_table mktoModule" id="bodyText52ac3f8f-781b-44de-a63d-3f5c6ed65720" align="center" width="600" border="0" cellspacing="0" cellpadding="0" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" class="em_aside" style="padding:0 50px;">
                                                                    <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:500px; margin-bottom: 20px;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" valign="top" class="mktoText" id="bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720" style="font-family:\'Roboto\', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;"><span style="font-weight: bold;">City:</span> '.$city.'</td>
                                                                                <td align="left" valign="top" class="mktoText" id="bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720" style="font-family:\'Roboto\', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;"><span style="font-weight: bold;">Region:</span> '.$region.'</td>
                                                                                <td align="left" valign="top" class="mktoText" id="bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720" style="font-family:\'Roboto\', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;"><span style="font-weight: bold;">Time:</span> '.$time.'</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="em_main_table mktoModule" id="bodyText52ac3f8f-781b-44de-a63d-3f5c6ed65720" align="center" width="600" border="0" cellspacing="0" cellpadding="0" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" class="em_aside" style="padding:0 50px;">
                                                                    <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:500px; margin-bottom: 20px;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" valign="top" class="mktoText" id="bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720" style="font-family:\'Roboto\', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;"><span style="font-weight: bold;">Device:</span> '.$device.'</td>
                                                                                <td align="left" valign="top" class="mktoText" id="bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720" style="font-family:\'Roboto\', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;"><span style="font-weight: bold;">Browser:</span> '.$browser.'</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="em_main_table mktoModule" id="bodyText52ac3f8f-781b-44de-a63d-3f5c6ed65720" align="center" width="600" border="0" cellspacing="0" cellpadding="0" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" class="em_aside" style="padding:0 50px;">
                                                                    <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:500px;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" valign="top" class="mktoText" id="bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720" style="font-family:\'Roboto\', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;"><span style="font-weight: bold;">Internet Services Provider:</span> '.$isp.'</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
            
                                    <table id="spacer01acf8a9b9-b644-46ac-8718-0c0266d5d23f" align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_main_table mktoModule" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td height="24" style="height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;">&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
            
                                    <table class="em_main_table mktoModule" id="bodyText52ac3f8f-781b-44de-a63d-3f5c6ed65720" align="center" width="600" border="0" cellspacing="0" cellpadding="0" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" class="em_aside" style="padding:0 50px;">
                                                                    <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:500px;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" valign="top" class="mktoText" id="bodyTextContent52ac3f8f-781b-44de-a63d-3f5c6ed65720" style="font-family:\'Roboto\', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;">Please if the BTC Address is not change by your click the link below to login
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                    
                                    <table id="spacer0136aee879-629d-41d6-b30c-70267e8ab7a3" align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_main_table mktoModule" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#FFFFFF">
                                                        <tbody>
                                                            <tr>
                                                                <td height="32" style="height: 32px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;">&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="em_main_table mktoModule" id="cta27cf44bb-9203-451f-b647-8d1e08c1bae8" align="center" width="600" border="0" cellspacing="0" cellpadding="0" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" class="em_aside" style="padding:0 50px;">
                                                                    <table border="0" cellspacing="0" cellpadding="0" align="left" bgcolor="#1a73e8" style="border-radius:4px;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center" valign="middle" height="40" class="em_white mktoText" id="ctaContent27cf44bb-9203-451f-b647-8d1e08c1bae8" style="font-family:\'Google Sans\', Arial, sans-serif; font-size:14px; letter-spacing: 0.44px; color:#ffffff; font-weight:500; border-radius:4px; padding: 0 20px;"><a href='.$link.'/login target="_blank" style="text-decoration:none; color:#ffffff; display:block; line-height:40px;">Login</a></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table id="spacer0136aee879-629d-41d6-b30c-70267e8ab7a3991920a5-20c7-4c0b-98a6-8c31cb652dce" align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_main_table mktoModule" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#FFFFFF">
                                                        <tbody>
                                                            <tr>
                                                                <td height="32" style="height: 32px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;">&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="em_main_table mktoModule" id="signoff" align="center" width="600" border="0" cellspacing="0" cellpadding="0" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" class="em_aside" style="padding:0 50px;">
                                                                    <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:500px;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" valign="top" class="mktoText" id="signoffContent" style="font-family:\'Google Sans\', Arial, sans-serif; font-size:14px; mso-line-height-rule:exactly; line-height:24px; color:#3C4043;"><strong>The Crypto Matrix Team</strong></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                   
                                    <table id="spacer014bce4d12-45fe-4a3a-b4f3-dc5a565f8bba" align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_main_table mktoModule" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px; border-radius: 0 0 3px 3px;" bgcolor="#FFFFFF">
                                                        <tbody>
                                                            <tr>
                                                                <td height="40" style="height: 40px; border-radius: 0 0 3px 3px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;">&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                  
                                    <table id="spacer015d5e649b-7b13-471b-b89c-b8bc31964f85af932f4e-81b2-4f73-9cb3-62f7e4c5d55a04dba9d9-eaae-4736-868d-798939474ed2" align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_main_table mktoModule" style="width:600px; max-width:600px;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" class="em_aside1">
                                                    <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="em_wrapper" style="width:600px; max-width:600px;" bgcolor="#F8F9FA">
                                                        <tbody>
                                                            <tr>
                                                                <td height="24" style="height: 24px; font-size:1px; line-height:1px; -webkit-text-size-adjust:none;">&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
            
                </body>
            
                </html>';

            sendMail($email, $subject, $message);

            QRcode::png($codeContents, $tempDir.''.$btc.'.png', QR_ECLEVEL_L, 5);
            $_SESSION['SuccessMessage'] = "Bitcoin Address Successfully Added";
            Redirect('wallet');
        }
    }
} 
