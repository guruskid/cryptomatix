<nav class="site-navigation navigation" id="site-navigation">
                <div class="container">
                    <div class="site-nav-inner">
                        <!-- Logo For ONLY Mobile display Starts -->
                        <a class="logo-mobile" href="./">
                            <img id="logo-mobile" class="img-responsive" src="images/logo-dark.png" alt="">
                        </a>
                        <!-- Logo For ONLY Mobile display Ends -->
                        <!-- Toggle Icon for Mobile Starts -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- Toggle Icon for Mobile Ends -->
                        <div class="collapse navbar-collapse navbar-responsive-collapse">
                            <!-- Main Menu Starts -->
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="./">Home</a></li>
                                <li><a href="about">About Us</a></li>
                                <!-- <li><a href="plan">Plan</a></li> -->
                                <li><a href="faq">FAQ</a></li>
                                <li><a href="contact">Contact</a></li>
                            </ul>
                            <!-- Main Menu Ends -->
                        </div>
                    </div>
                </div>
            </nav>