 <?php include_once '../include/function.php'; ?>
 <!DOCTYPE html>
 <!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
 <html lang="en">

 <head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>Dashboard</title>
   <link rel="shortcut icon" href="../images/favicon.png">

   <!-- Google Font: Source Sans Pro -->
   <link rel="stylesheet"
     href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
   <!-- Font Awesome Icons -->

   <link rel="stylesheet" href="css/font-awesome.min.css">
   <!-- Theme style -->
   <link rel="stylesheet" type="text/css" href="css/adminlte.min.css">
   <link rel="stylesheet" href="css/style.css">
   <link rel="stylesheet" href="css/toastr.min.css">
   <link rel="stylesheet" type="text/css" href="css/sweetalert2.min.css">
 </head>

 <body class="hold-transition layout-top-nav">
   <div class="wrapper">

     <!-- Navbar -->
     <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
       <div class="container">
         <a href="./" class="navbar-brand">
           <img src="img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
           <span class="brand-text font-weight-light">Crypto Matrix</span>
         </a>

         <!-- Right navbar links -->
         <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
           <li class="nav-item">
             <a class="nav-link text-danger" href="logout">
               <i class="fa fa-sign-out"></i> Logout
             </a>
           </li>
         </ul>
       </div>
     </nav>
     <!-- /.navbar -->

     <!-- Content Wrapper. Contains page content -->
     <div class="content-wrapper">
       <!-- Content Header (Page header) -->
       <div class="content-header">
         <div class="container">
           <?php logged_in(); echo Error_Message(); echo Success_Message(); ?>
           <div class="row mb-2">
             <div class="col-sm-6">
               <h1 class="m-0 text-dark"><?php Greeting() ?> <i class="fa fa-moon-o">,</i>
                 <span><?php echo $_SESSION['fname'] ?></span></h1>
             </div><!-- /.col -->
           </div><!-- /.row -->
         </div><!-- /.container-fluid -->
       </div>
       <!-- /.content-header -->

       <!-- Main content -->
       <section class="content">
         <div class="container">
           <?php

        $id = $_SESSION['id'];

        // Current Capital
        $cap = "SELECT * FROM capital WHERE user_id = '$id' ";
        $capsel = query($cap);
        while ($row = fetch_all($capsel)) {
          $capamount = $row['amount'];
        }

        // Current Earning
        $ear = "SELECT * FROM earning WHERE user_id = '$id' ";
        $earsel = query($ear);
        while ($row = fetch_all($earsel)) {
          $earamount = $row['amount'];
        }

        //Current Referal
        $ref = "SELECT * FROM referral WHERE user_id = '$id' ";
        $refsel = query($ref);
        while ($row = fetch_all($refsel)) {
          $refamount = $row['amount'];
        }

        // Current Withdrawn
        $with = "SELECT SUM(amount) AS sum FROM withdrawn WHERE user_id = '$id' ";
        $withsel = query($with);
        while ($row = fetch_all($withsel)) {
          $withamount = $row['sum'];
        }

        if (isset($_POST['btc'])) {
          $btcinput = $_POST['btc'];
          $date = date("Y-m-d");

          $btc = "SELECT * from earning WHERE user_id = '$id' ";
          $btcsel = query($btc);

          while ($row = fetch_all($btcsel)) {
            $amount = $row['amount'];
          }

          if (empty($btcinput)) {
            $_SESSION['ErrorMessage'] = "Bitcoin Address Can't be Empty";
            Redirect('index');
          } else {

            $send = "INSERT INTO pending(user_id, amount, date, btc, active) VALUES('$id', '$amount', '$date', '$btcinput', '0') ";
            $senddb = query($send);

            $del = "DELETE FROM earning WHERE user_id = '$id' ";
            $deldb = query($del);

            $_SESSION['SuccessMessage'] = "Withdrawn Successfully, Withdrawn Processing, Check Withdrawn History";
            Redirect('index');
          }


        }

        if (isset($_POST['bank'])) {
          $bankname = $_POST['bankname'];
          $accname = $_POST['accname'];
          $accnumber = $_POST['accnumber'];
          $date = date("Y-m-d");

          $bank = "SELECT * from earning WHERE user_id = '$id' ";
          $banksel = query($bank);

          while ($row = fetch_all($banksel)) {
            $amount = $row['amount'];
          }

          if (empty($bankname && $accname && $accnumber)) {
            $_SESSION['ErrorMessage'] = "Bank Details Can't be Empty";
            Redirect('index');
          } else {

            $banksend = "INSERT INTO pending(user_id, amount, bankname, accname, accnumber, date, active) VALUES('$id', '$amount', '$bankname', '$accname', $accnumber, '$date', '0')";
            $bankend = query($banksend);

            $del = "DELETE FROM earning WHERE user_id = '$id' ";
            $deldb = query($del);

            $_SESSION['SuccessMessage'] = "Withdrawn Successfully, Withdrawn Processing, Check Withdrawn History";
            Redirect('index');
          }
        }

         ?>
           <!-- Small boxes (Stat box) -->
           <div class="row">
             <div class="col-lg-3 col-6">
               <!-- small box -->
               <div class="small-box bg-info">
                 <div class="inner">
                   <h3>$<?php if (empty($capamount)) {
                  echo "0";
                } else {
                  echo $capamount;
                } ?></h3>
                   <p>Current Capitals</p>
                 </div>
               </div>
             </div>
             <!-- ./col -->
             <div class="col-lg-3 col-6">
               <!-- small box -->
               <div class="small-box bg-success">
                 <div class="inner">
                   <h3>$<?php if (empty($earamount)) {
                  echo "0";
                } else{
                  echo $earamount;
                } ?></h3>
                   <p>Current Earnings</p>
                 </div>
               </div>
             </div>
             <!-- ./col -->
             <div class="col-lg-3 col-6">
               <!-- small box -->
               <div class="small-box bg-warning">
                 <div class="inner">
                   <h3><?php if (empty($refamount)) {
                  echo "0";
                } else {
                  echo $refamount;
                } ?></h3>
                   <p>Referral Bonus</p>
                 </div>
               </div>
             </div>
             <!-- ./col -->
             <div class="col-lg-3 col-6">
               <!-- small box -->
               <div class="small-box bg-danger">
                 <div class="inner">
                   <h3><?php if (empty($withamount)) {
                  echo "0";
                } else {
                  echo $withamount;
                } ?></h3>
                   <p>Withdrawn</p>
                 </div>
               </div>
             </div>
             <!-- ./col -->
           </div>
           <div class="row">
             <div class="col-lg-6 col-6">
               <button class="btn btn-primary btn-lg btn-block" type="button" data-toggle="modal"
                 data-target="#modal-default"><i class="fa fa-bitcoin"></i> Depost</button>
             </div>
             <div class="col-lg-6 col-6">
               <button class="btn btn-success btn-lg btn-block" type="button" data-toggle="modal"
                 data-target="#withdraw"><i class="fa fa-send"></i> Withdraw</button>
             </div>
           </div>
         </div><!-- /.container-fluid -->
       </section>
       <section class="container my-3">
         <!-- TradingView Widget BEGIN -->
         <div class="tradingview-widget-container">
           <div id="tradingview_cc171" style="height: 610px"></div>
           <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
           <script type="text/javascript">
             new TradingView.widget({
               "autosize": true,
               "symbol": "BITBAY:BTCUSD",
               "interval": "1",
               "timezone": "Etc/UTC",
               "theme": "light",
               "style": "1",
               "locale": "en",
               "toolbar_bg": "#f1f3f6",
               "enable_publishing": false,
               "container_id": "tradingview_cc171"
             });
           </script>
         </div>
         <!-- TradingView Widget END -->
       </section>
       <!-- /.content -->
     </div>
     <!-- /.content-wrapper -->

     <div class="modal fade" id="modal-default">
       <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
           </div>
           <?php 
             $bitwall = "SELECT btc FROM wallet";
             $wallbit = query($bitwall);

             while ($row = fetch_all($wallbit)) {
               $bit = $row['btc'];
             }
              ?>
           <div class="modal-body">
             <h3 class="text-primary text-center text-uppercase">you are yet to fund your wallet</h3>
             <p class="text-muted text-center">Send payment to the wallet address below</p>
             <?php if (empty($bit)) {
               echo "No Bitcoin Address";
             } else {
              echo '<img src="../admin/temp/'. $bit.'.png" style="width:200px; height:200px;" class="img-responsive center-block d-block mx-auto">';
             } ?>
             <h6 class="text-center">BitCoin Wallet Address</h6>
             
             <h5 class="text-center font-weight-bold copyMe"><?php if (empty($bit)) {
               echo "No Bitcoin Address";
             } else {
              echo $bit;
             } ?></h5>
             <div class="text-center">
               <button class="btn btn-default copybtn">Copy Address</button>
               <button class="btn btn-success"><i class="fa fa-bitcoin"></i> <a
                   href="https://www.abra.com/buybitcoin/simplex/?crypto=BTC&fiat=USD&ref_id=abra-app" target="NEW"
                   style="color: #fff">Buy BitCoin</a></button>
             </div>
           </div>
           <div class="modal-footer justify-content-between">
             <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
           </div>
         </div>
         <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
     </div>
     <div class="modal fade" id="withdraw">
       <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
           </div>
           <div class="modal-body">
             <div class="card">
               <div class="card-header">
                 <h3 class="card-title">Withdrawn</h3>

                 <div class="card-tools">
                   <div class="">
                     <a href="<?php echo "history?userhis=$id" ?>"><button class="btn btn-info"><i
                           class="fa fa-pencil"></i> Withdraw History</button></a>
                   </div>
                 </div>

                 <div class="card-tools">
                 </div>
               </div>
               <!-- /.card-header -->
               <div class="card-body table-responsive p-0">
                 <table class="table table-hover text-nowrap">
                   <thead>
                     <tr>
                       <th>Amount</th>
                       <th>Earn Date</th>
                       <th>Action</th>
                     </tr>
                   </thead>
                   <tbody>
                     <?php 

                      $withdrawn = "SELECT * FROM earning WHERE user_id = '$id' ";
                      $withdrawnsel = query($withdrawn);

                      while ($row = fetch_all($withdrawnsel)) {
                        $amount = $row['amount'];
                        $date = $row['date'];

                        echo "<tr>";
                        echo "<td>$$amount</td>";
                        echo "<td>$date</td>";
                        echo "<td><button class=\"btn btn-success\" type=\"button\" data-toggle=\"modal\" data-target=\"#mode\">Withdraw</button></td>";
                        echo "</tr>";
                      }
                       ?>
                   </tbody>
                 </table>
               </div>
               <!-- /.card-body -->
             </div>
           </div>
           <div class="modal-footer justify-content-between">
             <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
           </div>
         </div>
         <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
     </div>

     <div class="modal fade" id="mode">
       <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
           </div>
           <div class="modal-body">
             <h2 class="text-center">WITHDRAWAL METHOD</h2>
             <div class="text-center">
               <button class="btn btn-info btn-lg " type="button" data-toggle="modal" data-target="#bitcoin"><i
                   class="fa fa-btc"> Bitcoin</i></button>
               <button class="btn btn-success btn-lg " type="button" data-toggle="modal" data-target="#bank"><i
                   class="fa fa-bank"> Bank</i></button>
             </div>

           </div>
           <div class="modal-footer justify-content-between">
             <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
           </div>
         </div>
         <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
     </div>
     <div class="modal fade" id="bitcoin">
       <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
           </div>
           <div class="modal-body">
             <h6 class="text-center">BitCoin Wallet Address</h6>
             <form action="" method="post">
               <div class="form-group">
                 <input class="form-control" type="text" name="btc" placeholder="Bitconin Address">
               </div>
               <div class="text-center">
                 <button type="submit" class="btn btn-success btn-lg btn-block my-3" name="save">Withdraw</button>
               </div>

             </form>
           </div>
           <div class="modal-footer justify-content-between">
             <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
           </div>
         </div>
         <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
     </div>
     <div class="modal fade" id="bank">
       <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
           </div>
           <div class="modal-body">
             <h6 class="text-center">Bank Details</h6>
             <form action="" method="post">
               <div class="form-group">
                 <input class="form-control" type="text" name="bankname" placeholder="Bank Name">
               </div>
               <div class="form-group">
                 <input class="form-control" type="text" name="accname" placeholder="Account Name">
               </div>
               <div class="form-group">
                 <input class="form-control" type="number" name="accnumber" placeholder="Account Number">
               </div>
               <div class="text-center">
                 <button type="submit" class="btn btn-success btn-lg btn-block my-3" name="bank">Withdraw</button>
               </div>

             </form>
           </div>
           <div class="modal-footer justify-content-between">
             <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
           </div>
         </div>
         <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
     </div>

     <!-- Main Footer -->
     <footer class="main-footer">
       <!-- To the right -->
       <div class="float-right d-none d-sm-inline">
         Crypto World
       </div>
       <!-- Default to the left -->
       <strong>Copyright &copy; 2020 All rights reserved.
     </footer>
   </div>
   <!-- ./wrapper -->

   <!-- REQUIRED SCRIPTS -->

   <!-- jQuery -->
   <script src="js/jquery.min.js"></script>
   <!-- Bootstrap 4 -->
   <script src="js/bootstrap.bundle.min.js"></script>
   <!-- AdminLTE App -->
   <script src="js/adminlte.min.js"></script>
   <script src="js/toastr.min.js"></script>
   <script type="text/javascript" src="js/sweetalert2.min.js"></script>
   <script src="js/main.js"></script>

   <script></script>
   <script>
     var copyEmailBtn = document.querySelector('.copybtn');
     copyEmailBtn.addEventListener('click', function (event) {
       // Select the email link anchor text  
       var emailLink = document.querySelector('.copyMe');
       var range = document.createRange();
       range.selectNode(emailLink);
       window.getSelection().addRange(range);

       try {
         // Now that we've selected the anchor text, execute the copy command  
         var successful = document.execCommand('copy');
         var msg = successful ? 'successful' : 'unsuccessful';
         toastr.success('BTC Address Copy was ' + msg);
       } catch (err) {
         toastr.success('Oops, unable to copy');
       }

       // Remove the selections - NOTE: Should use
       // removeRange(range) when it is supported  
       window.getSelection().removeAllRanges();
     });
   </script>

   <script type="text/javascript">
     const Toast = Swal.mixin({
       toast: true,
       position: 'top-end',
       showConfirmButton: false,
       timer: 3000,
       timerProgressBar: true
     });
   </script>
 </body>

 </html>
