<?php include_once '../include/function.php'; 
logged_in();
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Withdraw History</title>
  <link rel="shortcut icon" href="../images/favicon.png">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->

  <link rel="stylesheet" href="css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" type="text/css" href="css/adminlte.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/toastr.min.css">
  <link rel="stylesheet" type="text/css" href="css/sweetalert2.min.css">
</head>

<body class="hold-transition layout-top-nav">
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
      <div class="container">
        <a href="./" class="navbar-brand">
          <img src="img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
          <span class="brand-text font-weight-light">Crypto Matrix</span>
        </a>

        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
          <li class="nav-item">
            <a class="nav-link text-danger" href="logout">
              <i class="fa fa-sign-out"></i> Logout
            </a>
          </li>
        </ul>
      </div>
    </nav>
    <!-- /.navbar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container">
          <?php logged_in(); echo Error_Message(); echo Success_Message(); ?>
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark"><?php Greeting() ?> <i class="fa fa-moon-o"></i>
                <span><?php echo $_SESSION['fname'] ?></span></h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container">
          <!-- Small boxes (Stat box) -->
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Withdraw History</h3>

              <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                  <div class="input-group-append">
                    <button type="submit" class="btn btn-default"> <i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
              <table class="table table-hover text-nowrap">
                <thead>
                  <tr>
                    <th>Amount</th>
                    <th>Withdrawn Date</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 

                    $id = $_GET['userhis'];

                    $withdraw = "SELECT * FROM withdrawn WHERE user_id = '$id' ";
                    $withdrawdb = query($withdraw);

                    while ($row = fetch_all($withdrawdb)) {
                      $amount = $row['amount'];
                      $date = $row['date'];
                      $active = $row['active'];

                      echo "<tr>";
                      echo "<td>$$amount</td>";
                      echo "<td>$date</td>";
                      if ($active == 0) {
                        echo "<td><span class=\"text-danger\">Pending</span></td>";
                      } else {
                        echo "<td><span class=\"text-success\">Approved</span></td>";
                      }
                      echo "</tr>";

                    }

                    $pending = "SELECT * FROM pending WHERE user_id = '$id' ";
                    $pendingdb = query($pending);

                    while ($row = fetch_all($pendingdb)) {
                      $amount = $row['amount'];
                      $date = $row['date'];
                      $active = $row['active'];

                      echo "<tr>";
                      echo "<td>$$amount</td>";
                      echo "<td>$date</td>";
                      if ($active == 0) {
                        echo "<td><span class=\"text-danger\">Pending</span></td>";
                      } else {
                        echo "<td><span class=\"text-success\">Approved</span></td>";
                      }
                      echo "</tr>";

                    }
                     ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h3 class="text-primary text-center text-uppercase">you are yet to fund your wallet</h3>
            <p class="text-muted text-center">Send payment to the wallet address below</p>
            <img src="img/btc.jpeg" width="200" class="img-responsive center-block d-block mx-auto">
            <h6 class="text-center">BitCoin Wallet Address</h6>
            <h5 class="text-center font-weight-bold copyMe">xi989FND7dn723asdkzbZytwe723d7</h5>
            <div class="text-center">
              <button class="btn btn-default copybtn">Copy Address</button>
              <button class="btn btn-success"><i class="fa fa-bitcoin"></i> <a
                  href="https://www.abra.com/buybitcoin/simplex/?crypto=BTC&fiat=USD&ref_id=abra-app" target="NEW"
                  style="color: #fff">Buy BitCoin</a></button>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="withdraw">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Withdrawn</h3>

                <div class="card-tools">
                  <div class="">
                    <button class="btn btn-info"><i class="fa fa-pencil"></i> Withdraw History</button>
                  </div>
                </div>

                <div class="card-tools">
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>S/N</th>
                      <th>Name</th>
                      <th>Amount</th>
                      <th>Deposit Date</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Earnings</td>
                      <td>$6000</td>
                      <td>11-7-2014</td>
                      <td><button class="btn btn-success" type="button" data-toggle="modal"
                          data-target="#payment">Withdraw</button></td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Capitals</td>
                      <td>$12000</td>
                      <td>11-7-2014</td>
                      <td><button class="btn btn-success" type="button" data-toggle="modal"
                          data-target="#payment">Withdraw</button></td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Referral </td>
                      <td>$600</td>
                      <td>11-7-2014</td>
                      <td><button class="btn btn-success" type="button" data-toggle="modal"
                          data-target="#payment">Withdraw</button></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="payment">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h3 class="text-primary text-center text-uppercase">mode of withdrawer</h3>
            <div class="row text-center">
              <div class="col-md-6 col-6">
                <a href="#" data-toggle="modal" data-target="#bitcoin"><i class="fa fa-bitcoin"
                    style="font-size: 100px"></i></a>
                <p class="text-center">Bitcoin</p>
              </div>
              <div class="col-md-6 col-6">
                <a href="#" data-toggle="modal" data-target="#bank"><i class="fa fa-bank"
                    style="font-size: 100px"></i></a>
                <p class="text-center">Bank</p>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="bank">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h6 class="text-center">Bank Details</h6>
            <form action="" method="post">
              <div class="form-group">
                <input class="form-control" type="bankName" name="btc" placeholder="Bank Name">
              </div>
              <div class="form-group">
                <input class="form-control" type="aName" name="btc" placeholder="Account Name">
              </div>
              <div class="form-group">
                <input class="form-control" type="aNumber" name="btc" placeholder="Account Number">
              </div>
              <div class="text-center">
                <button type="button" class="btn btn-success btn-lg btn-block my-3" name="save">Withdraw</button>
              </div>

            </form>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="bitcoin">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h6 class="text-center">BitCoin Wallet Address</h6>
            <form action="" method="post">
              <div class="form-group">
                <input class="form-control" type="text" name="btc" placeholder="Bitconin Address">
              </div>
              <div class="text-center">
                <button type="button" class="btn btn-success btn-lg btn-block my-3" name="save">Withdraw</button>
              </div>

            </form>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="float-right d-none d-sm-inline">
        Crypto World
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; 2020 All rights reserved.
    </footer>
  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->

  <!-- jQuery -->
  <script src="js/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="js/adminlte.min.js"></script>
  <script src="js/toastr.min.js"></script>
  <script type="text/javascript" src="js/sweetalert2.min.js"></script>
  <script src="js/main.js"></script>

  <script></script>
  <script>
    var copyEmailBtn = document.querySelector('.copybtn');
    copyEmailBtn.addEventListener('click', function (event) {
      // Select the email link anchor text  
      var emailLink = document.querySelector('.copyMe');
      var range = document.createRange();
      range.selectNode(emailLink);
      window.getSelection().addRange(range);

      try {
        // Now that we've selected the anchor text, execute the copy command  
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        alert('BTC Address Copy was ' + msg);
      } catch (err) {
        alert('Oops, unable to copy');
      }

      // Remove the selections - NOTE: Should use
      // removeRange(range) when it is supported  
      window.getSelection().removeAllRanges();
    });
  </script>

  <script type="text/javascript">
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true
    });
  </script>
</body>

</html>
