<?php 

include_once '../include/function.php';

session_destroy();

if (isset($_COOKIE['email'])) {
    unset($_COOKIE['email']);
    setcookie('email', '', time() - 120);
}

// echo "<script>alert('You are logged out'); window.location.href='../login';</script>";
Redirect('../login');